import os

from apistar_sqlalchemy import database
from sqlalchemy import create_engine

from back_end.lib.sqlalchemy import ModelBase
from back_end.models.draft_results import DraftResults
from back_end.models.game_schedule import GameSchedule
from back_end.models.game_stat import GameStat
from back_end.models.league import League
from back_end.models.player import Player
from back_end.models.season_stat import SeasonStat
from back_end.models.stat_category import StatCategory
from back_end.models.status import Status
from back_end.models.target import Target
from back_end.models.team import Team
from back_end.models.team_players import TeamPlayer
from back_end.settings import DB_FILE

if os.path.isfile(DB_FILE):
    os.remove(DB_FILE)

Player
SeasonStat
GameStat
Status
Team
DraftResults
Target
League
StatCategory
GameSchedule
TeamPlayer

engine = create_engine('sqlite:///{file}'.format(file=DB_FILE), echo=True)
ModelBase.metadata.create_all(engine)

database.Session.configure(bind=engine)
session = database.Session()

# load_fixture(session, 'fixtures')
