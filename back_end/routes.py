from apistar import Route

from back_end.views.draft_results import draft_results_list, draft_results_create, draft_results_delete, \
    refresh_draft_results
from back_end.views.game_stat import game_stat_refresh
from back_end.views.player import player_list, player_read
from back_end.views.reports import hot_streak_view
from back_end.views.season_stat import season_stat_list
from back_end.views.status import status_update, reset_visible
from back_end.views.target import target_list
from back_end.views.team import team_list
from back_end.views.team_player import team_player_list

routes = [
    # player
    Route('/player/', method='GET', handler=player_list),
    Route('/player/{uid}/', method='GET', handler=player_read),

    # stats
    Route('/player/{uid}/season_stat/', method='GET', handler=season_stat_list),

    # status
    Route('/player/{player_id}/status/', method='PUT', handler=status_update),
    Route('/status_reset/', method='GET', handler=reset_visible),

    # draft results
    Route('/draft_results/', method='GET', handler=draft_results_list),
    Route('/draft_results/', method='PATCH', handler=refresh_draft_results),
    Route('/draft_results/', method='POST', handler=draft_results_create),
    Route('/draft_results/{uid}/', method='DELETE', handler=draft_results_delete),

    # teams
    Route('/team/', method='GET', handler=team_list),

    # targets
    Route('/target/', method='GET', handler=target_list),

    # game stats
    Route('/game_stats/', method='PATCH', handler=game_stat_refresh),

    # reports
    Route('/reports/hot_streak/', method='POST', handler=hot_streak_view),

    # team players
    Route('/team_players/', method='GET', handler=team_player_list),

]
