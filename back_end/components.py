from apistar_sqlalchemy.components import SQLAlchemySessionComponent

from back_end.settings import SQLALCHEMY_OPTIONS
from back_end.yahoo.component import YahooSessionComponent

components = [
    SQLAlchemySessionComponent(**SQLALCHEMY_OPTIONS),
    YahooSessionComponent('auth.json'),
]
