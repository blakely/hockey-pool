from typing import List

from apistar import App
from sqlalchemy.orm import Session

from back_end.models.season_stat import SeasonStat, SeasonStatType
from back_end.views.model_methods import model_list


# def stat_read(uid: int, session: Session, app: App) -> List[StatType]:
#     """
#     Get a list of all Stats
#     """
#     return model_read(Stat, uid, session, app=app, name='stat_list')


def season_stat_list(uid: int, session: Session, app: App) -> List[SeasonStatType]:
    """
    Get a list of all stats
    """
    return model_list(SeasonStat, session, filters={'player_id': uid}, app=app, name='stat_list')
