from apistar import App
from sqlalchemy.orm import Session

from back_end.models.status import StatusType, Status
from back_end.views.model_methods import model_update


def status_update(player_id: str, data: StatusType, session: Session, app: App) -> StatusType:
    """
    Set the status of a player
    """

    return model_update(Status, player_id, data, session, id_field='player_id')  # , app=app, name='player_list')


def reset_visible(session: Session) -> dict:
    """
    Reset all player visibility to True.
    """
    for row in session.query(Status).all():
        row.visible = True

    session.commit()

    return {'status': 'complete'}
