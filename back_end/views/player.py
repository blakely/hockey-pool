from typing import List

from apistar import App
from sqlalchemy.orm import Session

from back_end.models.player import PlayerType, Player
from back_end.models.season_stat import SeasonStat
from back_end.models.status import Status
from back_end.views.model_methods import model_read, get_url


def player_read(uid: int, session: Session, app: App) -> PlayerType:
    """
    Get a list of all Players
    """
    return model_read(Player, uid, session, app=app, name='player_list')


def model_list_player(session, filters: dict = None, **kwargs):
    data = session.query(Player, SeasonStat, Status).join(SeasonStat).join(Status)
    if filters:
        for key, val in filters.items():
            data = data.filter_by(**{key: val})

    url = get_url(**kwargs)
    r_data = {}
    for row in data.all():
        player, season_stats, status = row

        if player.id not in r_data:
            r_data[player.id] = player.as_type(url)
            r_data[player.id]['status'] = status.as_type()
            r_data[player.id]['season_stats'] = []

        r_data[player.id]['season_stats'].append(season_stats.as_type())

    return [val for key, val in r_data.items()]


def player_list(session: Session, app: App) -> List[PlayerType]:
    """
    Get a list of all players
    """
    return model_list_player(session, app=app, name='player_list')
