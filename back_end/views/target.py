from typing import List

from sqlalchemy.orm import Session

from back_end.models.league import League
from back_end.models.target import TargetType, Target
from back_end.views.model_methods import model_list, model_read


def target_list(session: Session) -> List[TargetType]:
    """
    Get a list of all targets.
    """
    r_data = [dict(x) for x in model_list(Target, session)]

    for data in r_data:
        data['league'] = model_read(League, data['league_id'], session)

    return r_data
