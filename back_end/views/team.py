from typing import List

from sqlalchemy.orm import Session

from back_end.models.team import TeamType, Team
from back_end.views.model_methods import model_list


def team_list(session: Session) -> List[TeamType]:
    """
    Get a list of all teams.
    """
    return model_list(Team, session)
