from apistar import http
from sqlalchemy.orm import Session

from back_end.yahoo.get_player_game_stats import get_all_player_game_stats
from back_end.yahoo.yahoo_session import YahooSession


def game_stat_refresh(ys: YahooSession, db_session: Session, ) -> http.JSONResponse:
    """
    Get a list of all stats
    """
    try:
        get_all_player_game_stats(ys, db_session)

    except Exception as e:
        return http.JSONResponse({'status': 'fail', 'description': str(e)}, status_code=400)

    return http.JSONResponse({'status': 'complete'}, status_code=200)
