from typing import List

from sqlalchemy.orm import Session

from back_end.models.team_players import TeamPlayerType, TeamPlayer
from back_end.views.model_methods import model_list
from back_end.yahoo.get_team_players import get_all_team_players
from back_end.yahoo.yahoo_session import YahooSession


def team_player_list(ys: YahooSession, session: Session) -> List[TeamPlayerType]:
    """
    Get a list of all players
    """
    get_all_team_players(ys, session)
    return model_list(TeamPlayer, session)
