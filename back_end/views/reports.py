from apistar import http, types, validators
from sqlalchemy.orm import Session

from back_end.reports.hot_streak import hot_streak


class GameCountType(types.Type):
    game_count = validators.Integer(allow_null=True)


def hot_streak_view(params: GameCountType, session: Session) -> http.JSONResponse:
    """
    Get Hot Streaks report.
    """

    try:
        return http.JSONResponse(hot_streak(session, params.game_count), status_code=200)

    except Exception as e:
        return http.JSONResponse({'status': 'fail', 'description': str(e)}, status_code=400)
