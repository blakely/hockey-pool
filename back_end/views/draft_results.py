from typing import List

from apistar import App, http
from sqlalchemy.orm import Session

from back_end.models.draft_results import DraftResultsType, DraftResults
from back_end.settings import DEBUG
from back_end.views.model_methods import model_list, model_create, model_delete
from back_end.yahoo.yahoo_session import yahoo_session, league_url as league_url_func, get_league_id


def refresh_draft_results(session: Session) -> http.JSONResponse:
    """
    Refresh draft results from yahoo.
    """
    league_url = league_url_func()
    # test url
    if DEBUG:
        # for this work on the front end
        # modify function compile_teams_results() to replace the league id with the current one.
        league_id = get_league_id(2017)
        league_url = 'https://fantasysports.yahooapis.com/fantasy/v2/league/{league_id}/'.format(league_id=league_id)

    url = league_url + 'draftresults?format=json'

    ys = yahoo_session()
    response = ys.get(url)
    data = response.json()
    # save_data(data)
    draft_results = data['fantasy_content']['league'][1]['draft_results']

    # clear table
    for instance in session.query(DraftResults).all():
        session.delete(instance)
    session.commit()

    if len(draft_results) > 0:
        draft_results.pop('count')

        for key, val in draft_results.items():
            results = val['draft_result']
            params = {
                'id': results['pick'],
                'round': results['round'],
                'team_id': results['team_key'] if 'team_key' in results else '',
                'player_id': results['player_key'] if 'player_key' in results else '',
            }
            instance = DraftResults(**params)

            session.merge(instance)

        session.commit()
    return http.JSONResponse({'status': 'complete'}, status_code=200)


def draft_results_list(session: Session, app: App) -> List[DraftResultsType]:
    """
    Get a list of all Players
    """
    return model_list(DraftResults, session, app=app, name='draft_results_list')


def draft_results_create(draft_result: DraftResultsType, session: Session, app: App) -> DraftResultsType:
    query = session.query(DraftResults).filter(DraftResults.round.isnot(None)).all()
    for instance in query:
        session.delete(instance)
    session.commit()
    return model_create(DraftResults, draft_result, session, app=app, name='draft_results_list')


def draft_results_delete(uid: int, session: Session) -> http.JSONResponse:
    """
    Delete draft result
    """
    if model_delete(DraftResults, uid, session):
        return http.JSONResponse({}, status_code=204)
    else:
        return http.JSONResponse({}, status_code=404)
