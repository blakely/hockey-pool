def get_url(app=None, name=None):
    if not app or not name:
        return None
    return app.reverse_url(name=name)


def model_read(model_class, instance_id, session, id_field='id', **kwargs):
    instance = session.query(model_class).filter(getattr(model_class, id_field) == instance_id).first()
    url = get_url(**kwargs)
    return instance.as_type(url)


def model_list(model_class, session, filters: dict = None, **kwargs):
    data = session.query(model_class)
    if filters:
        for key, val in filters.items():
            data = data.filter_by(**{key: val})

    data = data.all()
    url = get_url(**kwargs)
    return [x.as_type(url) for x in data]


def model_update(model_class, instance_id, data, session, id_field='id', **kwargs):
    instance = session.query(model_class).filter(getattr(model_class, id_field) == instance_id).first()

    for key, val in data.items():
        if key != id_field:
            setattr(instance, key, getattr(data, key))

    # session.commit()
    url = get_url(**kwargs)
    return instance.as_type(url)


def model_create(model_class, data, session, **kwargs):
    model = model_class(**data)
    session.add(model)
    session.commit()
    url = get_url(**kwargs)
    return model.as_type(url)


def model_delete(model_class, instance_id, session, id_field='id'):
    instance = session.query(model_class).filter(getattr(model_class, id_field) == instance_id).first()
    #print(instance)
    session.delete(instance)
    # session.commit()
    return True
