from apistar_sqlalchemy import database
from sqlalchemy import create_engine

from back_end.settings import DB_FILE

engine = create_engine('sqlite:///{file}'.format(file=DB_FILE), echo=True)

database.Session.configure(bind=engine)


# session = database.Session()
def db_session():
    return database.Session()
