from sqlalchemy.ext.declarative import declarative_base

from back_end.lib.model_validation_mixin import TypeValidationMixin


class MixinCollection(TypeValidationMixin):
    pass


ModelBase = declarative_base(cls=MixinCollection)