from apistar.exceptions import ValidationError


class TypeValidationMixin(object):
    _type = None

    def is_valid(self):
        try:
            return self.as_type()

        except ValidationError as e:
            return False

    def validation_errors(self):
        try:
            self.as_type()
            return None

        except ValidationError as e:
            return e

    def as_type(self, url=None, id_field='id', skip_id=False):
        if url:
            data = dict(self._type(self))

            url_extra = ''
            if not skip_id:
                url_extra = str(data[id_field]) + '/'

            data['url'] = url + url_extra
            return data
        else:
            return self._type(self)
