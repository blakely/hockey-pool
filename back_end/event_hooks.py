from apistar_sqlalchemy.event_hooks import SQLAlchemyTransactionHook

event_hooks = [
    SQLAlchemyTransactionHook(),
]
