from apistar import types, validators
from sqlalchemy import Column, String, Boolean

from back_end.lib.sqlalchemy import ModelBase


class StatCategoryType(types.Type):
    id = validators.String(max_length=50, allow_null=True, default=None)
    name = validators.String(max_length=255, allow_null=True)
    display_name = validators.String(max_length=255, allow_null=True)


class StatCategory(ModelBase):
    __tablename__ = 'stat_category'
    _type = StatCategoryType

    id = Column(String(50), primary_key=True)
    name = Column(String(255))
    display_name = Column(String(255))
    tracked = Column(Boolean, default=False)

    def __repr__(self):
        return "<StatCategory(id={id}, name='{name}')>".format(id=self.id,
                                                               name=self.name)
