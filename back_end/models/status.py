from apistar import types, validators
from sqlalchemy import Column, Integer, Boolean, ForeignKey, String
from sqlalchemy.orm import relationship

from back_end.lib.sqlalchemy import ModelBase


class StatusType(types.Type):
    player_id = validators.String(max_length=50, allow_null=True)
    visible = validators.Boolean()


class Status(ModelBase):
    __tablename__ = 'status'
    _type = StatusType

    player_id = Column(String(50), ForeignKey('player.id'), primary_key=True)
    visible = Column(Boolean(), default=False)
    # owner= todo

    player = relationship("Player", back_populates="status")

    def __repr__(self):
        return "<Status(player_id={player_id}, visible='{visible}')>".format(player_id=self.player_id,
                                                                             visible=self.visible)
