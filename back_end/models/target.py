from apistar import types, validators
from sqlalchemy import Column, ForeignKey, String
from sqlalchemy.orm import relationship

from back_end.lib.sqlalchemy import ModelBase
from back_end.models.base_stat import BaseTrackedStatType, BaseTrackedStat


class TargetType(BaseTrackedStatType, types.Type):
    league_id = validators.String(max_length=50, allow_null=True)


class Target(BaseTrackedStat, ModelBase):
    __tablename__ = 'target'
    _type = TargetType

    league_id = Column(String(50), ForeignKey('league.id'), primary_key=True)

    league = relationship("League", back_populates="target")

    def __repr__(self):
        return "<Target(league_id={league_id})>".format(league_id=self.league_id)
