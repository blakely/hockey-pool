from apistar import types, validators
from sqlalchemy import Column, String, Boolean
from sqlalchemy.orm import relationship

from back_end.lib.sqlalchemy import ModelBase


class PlayerType(types.Type):
    id = validators.String(max_length=50, allow_null=True, default=None)
    key = validators.String(max_length=50, allow_null=True)
    name = validators.String(max_length=255, allow_null=True)
    team = validators.String(max_length=255, allow_null=True)
    pos_c = validators.Boolean()
    pos_w = validators.Boolean()
    pos_d = validators.Boolean()
    pos_g = validators.Boolean()

    url = validators.String(max_length=255, allow_null=True)


class Player(ModelBase):
    __tablename__ = 'player'
    _type = PlayerType

    id = Column(String(50), primary_key=True)
    key= Column(String(50))
    name = Column(String(255))
    team = Column(String(255))
    pos_c = Column(Boolean(), default=False)
    pos_w = Column(Boolean(), default=False)
    pos_d = Column(Boolean(), default=False)
    pos_g = Column(Boolean(), default=False)

    url = Column(String(255))

    season_stats = relationship("SeasonStat", back_populates="player")
    game_stats = relationship("GameStat", back_populates="player")
    status = relationship("Status", back_populates="player")

    draft_results = relationship("DraftResults", back_populates="player")

    def __repr__(self):
        return "<Player(id={id}, name='{name}')>".format(id=self.id,
                                                         name=self.name)
