from apistar import types, validators
from sqlalchemy import Column, Integer, String, Date
from sqlalchemy.orm import relationship

from back_end.lib.sqlalchemy import ModelBase


class LeagueType(types.Type):
    id = validators.String(max_length=50, allow_null=True)
    name = validators.String(max_length=255, allow_null=True)
    game_id = validators.Integer(allow_null=True)
    league_id = validators.Integer(allow_null=True)
    year = validators.Integer(allow_null=True)
    team_count = validators.Integer(allow_null=True)
    start_date = validators.Date(allow_null=True)
    end_date = validators.Date(allow_null=True)


class League(ModelBase):
    __tablename__ = 'league'
    _type = LeagueType

    id = Column(String(50), primary_key=True)
    name = Column(String(255))
    game_id = Column(Integer)
    league_id = Column(Integer)
    year = Column(Integer)
    team_count = Column(Integer)
    start_date = Column(String(50))
    end_date = Column(String(50))

    target = relationship("Target", back_populates="league")

    def __repr__(self):
        return "<League(id={id}, year='{year}')>".format(id=self.id,
                                                         year=self.year)
