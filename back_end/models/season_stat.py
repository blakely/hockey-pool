from apistar import types, validators
from sqlalchemy import Column, String, Integer, ForeignKey, UniqueConstraint
from sqlalchemy.orm import relationship

from back_end.lib.sqlalchemy import ModelBase
from back_end.models.base_stat import BaseStat, BaseStatType


class SeasonStatType(BaseStatType, types.Type):
    id = validators.Integer(allow_null=True, default=None)
    player_id = validators.String(max_length=50, allow_null=True)
    year = validators.Integer(allow_null=True)


class SeasonStat(BaseStat, ModelBase):
    __tablename__ = 'season_stat'
    _type = SeasonStatType
    __table_args__ = (UniqueConstraint('player_id', 'year'),)

    id = Column(Integer, primary_key=True)
    player_id = Column(String(50), ForeignKey('player.id'))
    year = Column(Integer())

    player = relationship("Player", back_populates="season_stats")

    def __repr__(self):
        return "<SeasonStat(player_id={player_id}, year='{year}')>".format(player_id=self.player_id,
                                                                           year=self.year)
