from apistar import types, validators
from sqlalchemy import Column, String
from sqlalchemy.orm import relationship

from back_end.lib.sqlalchemy import ModelBase


class TeamType(types.Type):
    id = validators.String(max_length=50, allow_null=True, default=None)
    name = validators.String(max_length=255, allow_null=True)
    email = validators.String(max_length=255, allow_null=True)
    nick_name = validators.String(max_length=255, allow_null=True)
    logo_url = validators.String(max_length=255, allow_null=True)

    # url = validators.String(max_length=255, allow_null=True)


class Team(ModelBase):
    __tablename__ = 'team'
    _type = TeamType

    id = Column(String(50), primary_key=True)
    name = Column(String(255))
    email = Column(String(255))
    nick_name = Column(String(255))
    logo_url = Column(String(255))

    draft_results = relationship("DraftResults", back_populates="team")

    def __repr__(self):
        return "<Team(id={id}, name='{name}')>".format(id=self.id,
                                                       name=self.name)
