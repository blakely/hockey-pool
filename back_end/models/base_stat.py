from apistar import types
from sqlalchemy import Column

from back_end.yahoo.stats import stat_mapping_full


class BaseStatType(types.Type):
    for item in stat_mapping_full():
        locals()[item['field']] = item['validator']


class BaseStat(object):
    for item in stat_mapping_full():
        locals()[item['field']] = Column(item['field_type'], default=None)


class BaseTrackedStatType(types.Type):
    for item in stat_mapping_full():
        if item['tracked']:
            locals()[item['field']] = item['validator']


class BaseTrackedStat(object):
    for item in stat_mapping_full():
        if item['tracked']:
            locals()[item['field']] = Column(item['field_type'], default=None)
