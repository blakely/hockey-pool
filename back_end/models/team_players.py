from apistar import types, validators
from sqlalchemy import Column, String, ForeignKey

from back_end.lib.sqlalchemy import ModelBase


class TeamPlayerType(types.Type):
    player_id = validators.String(max_length=50, allow_null=True)
    team_id = validators.String(max_length=50, allow_null=True)
    last_updated = validators.Date()


class TeamPlayer(ModelBase):
    __tablename__ = 'team_player'
    _type = TeamPlayerType

    player_id = Column(String(50), ForeignKey('player.id'), primary_key=True)
    team_id = Column(String(50), ForeignKey('team.id'))
    last_updated = Column(String(50))

    # draft_results = relationship("DraftResults", back_populates="team")

    def __repr__(self):
        return "<TeamPlayer(player_id='{player_id}', team_id='{team_id}')>".format(team_id=self.team_id,
                                                                                   player_id=self.player_id)
