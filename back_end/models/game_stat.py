from apistar import types, validators
from sqlalchemy import Column, String, Integer, ForeignKey, UniqueConstraint, DateTime
from sqlalchemy.orm import relationship

from back_end.lib.sqlalchemy import ModelBase
from back_end.models.base_stat import BaseStat, BaseStatType


class GameStatType(BaseStatType, types.Type):
    id = validators.Integer(allow_null=True, default=None)
    player_id = validators.String(max_length=50)
    date = validators.Date()
    last_updated = validators.Date()


class GameStat(BaseStat, ModelBase):
    __tablename__ = 'game_stat'
    _type = GameStatType
    __table_args__ = (UniqueConstraint('player_id', 'date'),)

    id = Column(Integer, primary_key=True)
    player_id = Column(String(50), ForeignKey('player.id'))
    date = Column(String(50))
    last_updated = Column(String(50))

    player = relationship("Player", back_populates="game_stats")

    def __repr__(self):
        return "<GameStat(player_id='{player_id}', date={date})>".format(player_id=self.player_id, date=self.date)
