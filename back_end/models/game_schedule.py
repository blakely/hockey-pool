from apistar import types, validators
from sqlalchemy import Column, String, Integer

from back_end.lib.sqlalchemy import ModelBase


class GameScheduleType(types.Type):
    id = validators.Integer(allow_null=True, default=None)

    date = validators.Date()
    date_time = validators.DateTime(allow_null=True)

    home = validators.String(max_length=250, allow_null=True)
    away = validators.String(max_length=250, allow_null=True)

    home_id = validators.Integer(allow_null=True)
    away_id = validators.Integer(allow_null=True)


class GameSchedule(ModelBase):
    __tablename__ = 'game_schedule'
    _type = GameScheduleType

    id = Column(Integer, primary_key=True)

    date = Column(String(50))
    date_time = Column(String(50))

    home = Column(String(50))
    away = Column(String(50))

    home_id = Column(Integer)  # nhl.com id
    away_id = Column(Integer)  # nhl.com id

    def __repr__(self):
        return "<GameSchedule(id={id}, date={date})>".format(id=self.id, date=self.date)
