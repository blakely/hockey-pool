from apistar import types, validators
from sqlalchemy import Column, Integer, ForeignKey, String
from sqlalchemy.orm import relationship

from back_end.lib.sqlalchemy import ModelBase


class DraftResultsType(types.Type):
    id = validators.Integer(allow_null=True, default=None)
    round = validators.Integer(allow_null=True)
    team_id = validators.String(max_length=50, allow_null=True)
    player_id = validators.String(max_length=50, allow_null=True)

    # url = validators.String(max_length=255, allow_null=True)


class DraftResults(ModelBase):
    __tablename__ = 'draft_results'
    _type = DraftResultsType

    id = Column(Integer, primary_key=True)  # pick number
    round = Column(Integer)
    team_id = Column(String(50), ForeignKey('team.id'))
    player_id = Column(String(50), ForeignKey('player.id'))

    team = relationship("Team", back_populates="draft_results")
    player = relationship("Player", back_populates="draft_results")

    def __repr__(self):
        return "<DraftResults(id={id}, round='{round}')>".format(id=self.id,
                                                                 round=self.round)
