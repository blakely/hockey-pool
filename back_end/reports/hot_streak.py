from back_end.models.game_stat import GameStat


def hot_streak(db_session, limit=3):
    sum_stats = ['gp', 'g', 'a', 'p', 'plus_minus', 'pim', 'ppp', 'shp', 'gwg', 'hit', 'blk', ]
    player_data = {x.player_id: {y: 0 for y in sum_stats} for x in
                   db_session.query(GameStat.player_id).distinct().all()}

    # player_data = {'386.p.4849': {y: 0 for y in sum_stats}}  # to test
    for player_id, player_stats in player_data.items():

        stats = db_session.query(GameStat) \
            .filter(GameStat.player_id == player_id) \
            .order_by(GameStat.date.desc()) \
            .limit(limit).all()

        for stat in stats:
            for s in sum_stats:
                player_stats[s] += getattr(stat, s) or 0

    return player_data
