import requests

from back_end.models.game_schedule import GameSchedule
from back_end.models.league import League
from back_end.yahoo.yahoo_session import get_league_id


def get_game_schedule(db_session, start_date, end_date):
    base_url = 'https://statsapi.web.nhl.com/api/v1/schedule'
    url = base_url + '?startDate={start_date}&endDate={end_date}'.format(start_date=start_date, end_date=end_date)
    response = requests.get(url)

    data = response.json()

    for date in data['dates']:

        for game in date['games']:
            if game['gameType'] == 'R':  # regular season game
                params = {
                    'id': game['gamePk'],
                    'date': date['date'],
                    'date_time': game['gameDate'],
                    'home': game['teams']['home']['team']['name'],
                    'away': game['teams']['away']['team']['name'],
                    'home_id': game['teams']['home']['team']['id'],
                    'away_id': game['teams']['away']['team']['id'],
                }

                instance = GameSchedule(**params)
                db_session.merge(instance)

    db_session.commit()


def get_all_game_schedule(db_session, league_id=None):
    if not league_id:
        league_id = get_league_id()
    start_date, end_date = db_session.query(League.start_date, League.end_date).filter(League.id == league_id).one()

    get_game_schedule(db_session, start_date, end_date)
