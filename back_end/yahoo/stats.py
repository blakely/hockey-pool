import copy

from apistar import validators
from sqlalchemy import Float, Integer, String

stat_mapping = [
    {'id': '0', 'display_name': 'GP', 'name': 'Games Played', 'field': 'gp', 'tracked': False},
    {'id': '1', 'display_name': 'G', 'name': 'Goals', 'field': 'g', 'tracked': True},
    {'id': '2', 'display_name': 'A', 'name': 'Assists', 'field': 'a', 'tracked': True},
    {'id': '3', 'display_name': 'P', 'name': 'Points', 'field': 'p', 'tracked': False},
    {'id': '4', 'display_name': '+/-', 'name': 'Plus/Minus', 'field': 'plus_minus', 'tracked': True},
    {'id': '5', 'display_name': 'PIM', 'name': 'Penalty Minutes', 'field': 'pim', 'tracked': True},
    {'id': '6', 'display_name': 'PPG', 'name': 'Powerplay Goals', 'field': 'ppg', 'tracked': False},
    {'id': '7', 'display_name': 'PPA', 'name': 'Powerplay Assists', 'field': 'ppa', 'tracked': False},
    {'id': '8', 'display_name': 'PPP', 'name': 'Powerplay Points', 'field': 'ppp', 'tracked': True},
    {'id': '9', 'display_name': 'SHG', 'name': 'Shorthanded Goals', 'field': 'shg', 'tracked': False},
    {'id': '10', 'display_name': 'SHA', 'name': 'Shorthanded Assists', 'field': 'sha', 'tracked': False},
    {'id': '11', 'display_name': 'SHP', 'name': 'Shorthanded Points', 'field': 'shp', 'tracked': True},
    {'id': '12', 'display_name': 'GWG', 'name': 'Game-Winning Goals', 'field': 'gwg', 'tracked': True},
    {'id': '13', 'display_name': 'GTG', 'name': 'Game-Tying Goals', 'field': 'gtg', 'tracked': False},
    {'id': '14', 'display_name': 'SOG', 'name': 'Shots on Goal', 'field': 'sog', 'tracked': False},
    {'id': '15', 'display_name': 'SH%', 'name': 'Shooting Percentage', 'field': 'sh_p', 'tracked': False},
    {'id': '16', 'display_name': 'FW', 'name': 'Faceoffs Won', 'field': 'fw', 'tracked': False},
    {'id': '17', 'display_name': 'FL', 'name': 'Faceoffs Lost', 'field': 'fl', 'tracked': False},
    {'id': '18', 'display_name': 'GS', 'name': 'Games Started', 'field': 'gs', 'tracked': False},
    {'id': '19', 'display_name': 'W', 'name': 'Wins', 'field': 'w', 'tracked': True},
    {'id': '20', 'display_name': 'L', 'name': 'Losses', 'field': 'l', 'tracked': False},
    {'id': '21', 'display_name': 'T', 'name': 'Ties', 'field': 't', 'tracked': False},
    {'id': '22', 'display_name': 'GA', 'name': 'Goals Against', 'field': 'ga', 'tracked': True},
    {'id': '23', 'display_name': 'GAA', 'name': 'Goals Against Average', 'field': 'gaa', 'tracked': True},
    {'id': '24', 'display_name': 'SA', 'name': 'Shots Against', 'field': 'sa', 'tracked': True},
    {'id': '25', 'display_name': 'SV', 'name': 'Saves', 'field': 'sv', 'tracked': True},
    {'id': '26', 'display_name': 'SV%', 'name': 'Save Percentage', 'field': 'sv_p', 'tracked': True},
    {'id': '27', 'display_name': 'SHO', 'name': 'Shutouts', 'field': 'sho', 'tracked': True},
    {'id': '28', 'display_name': 'TOI', 'name': 'Time on Ice', 'field': 'toi', 'tracked': False},
    {'id': '29', 'display_name': 'GP', 'name': 'F/D Games', 'field': 'gp_p', 'tracked': False},
    {'id': '30', 'display_name': 'GP', 'name': 'Goalie Games', 'field': 'gp_g', 'tracked': False},
    {'id': '31', 'display_name': 'HIT', 'name': 'Hits', 'field': 'hit', 'tracked': True},
    {'id': '32', 'display_name': 'BLK', 'name': 'Blocks', 'field': 'blk', 'tracked': True},
    {'id': '33', 'display_name': 'TOI', 'name': 'Time on Ice', 'field': 'toi', 'tracked': False},
    {'id': '34', 'display_name': 'TOI/G', 'name': 'Average Time on Ice', 'field': 'toi_g', 'tracked': False},
]
stat_mapping_extra = {
    '0': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '1': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '2': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '3': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '4': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '5': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '6': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '7': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '8': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '9': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '10': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '11': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '12': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '13': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '14': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '15': {'field_type': Float, 'validator': validators.Number(allow_null=True)},
    '16': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '17': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '18': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '19': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '20': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '21': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '22': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '23': {'field_type': Float, 'validator': validators.Number(allow_null=True)},
    '24': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '25': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '26': {'field_type': Float, 'validator': validators.Number(allow_null=True)},
    '27': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '28': {'field_type': String(50), 'validator': validators.String(max_length=255, allow_null=True)},
    '29': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '30': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '31': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '32': {'field_type': Integer, 'validator': validators.Integer(allow_null=True)},
    '33': {'field_type': String(50), 'validator': validators.String(max_length=255, allow_null=True)},
    '34': {'field_type': String(50), 'validator': validators.String(max_length=255, allow_null=True)},
}


def stat_mapping_dict(key='id'):
    data = {}
    for stat in stat_mapping:
        data[stat[key]] = stat

    return data


def stat_mapping_full():
    mapping = copy.deepcopy(stat_mapping)

    for item in mapping:
        for key, val in stat_mapping_extra[item['id']].items():
            item[key] = val

    return mapping
