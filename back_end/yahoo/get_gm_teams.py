from back_end.models.team import Team
from back_end.yahoo.yahoo_session import league_url


def get_gm_teams(ys, db_session):
    url = league_url() + 'teams?format=json'

    response = ys.get(url)
    data = response.json()
    # save_data(data)
    teams = data['fantasy_content']['league'][1]['teams']

    teams.pop('count', None)

    for key, val in teams.items():
        params = {}
        team_data = val['team'][0]

        for d in team_data:
            if 'team_key' in d:
                params['id'] = d['team_key']
            if 'managers' in d:
                info = d['managers'][0]['manager']
                # params['id'] = int(info['manager_id'])
                params['name'] = info['nickname']
                params['email'] = info['email'] if 'email' in info else ''

            if 'team_logos' in d:
                params['logo_url'] = d['team_logos'][0]['team_logo']['url']

            if 'name' in d:
                params['nick_name'] = d['name']

        instance = Team(**params)
        db_session.merge(instance)

    db_session.commit()
