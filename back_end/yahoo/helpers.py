def list_to_dict(list_data):
    r_dict = {}
    for item in list_data:
        if isinstance(item, dict):
            for key, val in item.items():
                r_dict[key] = val

    return r_dict


def stats_to_dict(stats):
    r_dict = {}
    for item in stats:
        if isinstance(item, dict):
            r_dict[item['stat']['stat_id']] = item['stat']['value']

    return r_dict


def chunks(data, size):
    for i in range(0, len(data), size):
        yield data[i:i + size]
