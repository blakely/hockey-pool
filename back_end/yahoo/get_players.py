from back_end.models.player import Player
from back_end.models.status import Status
from back_end.yahoo.yahoo_session import league_url


def get_player_data(ys, db_session, start):
    url = league_url() + 'players;status=ALL;sort=OR;start={start}?format=json'
    response = ys.get(url.format(start=start))
    data = response.json()
    # save_data(data)
    players = data['fantasy_content']['league'][1]['players']
    if len(players) == 0:
        return False
    players.pop('count', None)

    process_players(db_session, players)
    return True


def get_all_players(ys, db_session, max_players=None):
    step = 0
    again = True

    while again:
        again = get_player_data(ys, db_session, step)
        step += 25
        if max_players and step > max_players:
            again = False


def process_players(db_session, players):
    for index, player in players.items():
        player = player['player']
        metadata = player[0]

        params = {}

        for item in metadata:
            if 'player_key' in item:
                params['id'] = item['player_key']
            elif 'player_id' in item:
                params['key'] = item['player_id']
            elif 'name' in item:
                params['name'] = item['name']['full']
            elif 'editorial_team_abbr' in item:
                params['team'] = item['editorial_team_abbr']
            elif 'eligible_positions' in item:
                for pos in item['eligible_positions']:
                    if pos['position'] == 'C':
                        params['pos_c'] = True
                    elif pos['position'] == 'W':
                        params['pos_w'] = True
                    elif pos['position'] == 'D':
                        params['pos_d'] = True
                    elif pos['position'] == 'G':
                        params['pos_g'] = True

        player_instance = Player(**params)
        db_session.merge(player_instance)

        status_instance = Status(player_id=params['id'], visible=True)
        db_session.merge(status_instance)

    db_session.commit()
