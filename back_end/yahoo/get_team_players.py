import datetime

from back_end.models.team import Team
from back_end.models.team_players import TeamPlayer
from back_end.yahoo.helpers import list_to_dict
from back_end.yahoo.yahoo_session import BASE_URL


def get_team_players(ys, db_session, team_id):
    """
    get a team's players
    """
    url = BASE_URL + 'team/{team_id}/roster?format=json'.format(team_id=team_id)

    response = ys.get(url)
    data = response.json()
    # save_data(data)

    players = data['fantasy_content']['team'][1]['roster']['0']['players']
    players.pop('count', None)

    delete_existing(db_session, team_id)

    for key, val in players.items():
        player = list_to_dict(val['player'][0])

        params = {
            'player_id': player['player_key'],
            'team_id': team_id,
            'last_updated': (datetime.date.today()).strftime('%Y-%m-%d'),
        }
        instance = TeamPlayer(**params)
        db_session.merge(instance)

    db_session.commit()


def get_all_team_players(ys, db_session):
    today = (datetime.date.today()).strftime('%Y-%m-%d')
    last_updated = db_session.query(TeamPlayer.last_updated).first().last_updated

    if last_updated != today:
        for team in db_session.query(Team).all():
            get_team_players(ys, db_session, team.id)


def delete_existing(db_session, team_id):
    db_session.query(TeamPlayer).filter(TeamPlayer.team_id == team_id).delete()

    db_session.commit()
