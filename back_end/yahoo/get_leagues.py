from back_end.models.league import League
from back_end.yahoo.yahoo_session import BASE_URL


def get_leagues(ys, db_session):
    """
    Get historical/current league data
    """
    # retrieve game_id for the previous season
    url = BASE_URL + 'users;use_login=1/games/leagues?format=json'
    response = ys.get(url)
    data = response.json()

    games = data['fantasy_content']['users']['0']['user'][1]['games']
    games.pop('count', None)

    for key, val in games.items():
        game_data = val['game'][0]
        league_data = val['game'][1]['leagues']['0']['league'][0]
        params = {
            'id': league_data['league_key'],
            'name': league_data['name'],
            'game_id': int(game_data['game_id']),
            'league_id': int(league_data['league_id']),
            'year': int(league_data['season']),
            'team_count': int(league_data['num_teams']),
            'start_date': league_data['start_date'],
            'end_date': league_data['end_date'],
        }

        instance = League(**params)
        db_session.merge(instance)

    db_session.commit()
