import datetime
import json

from yahoo_oauth import OAuth2

from back_end.models.league import League

BASE_URL = 'https://fantasysports.yahooapis.com/fantasy/v2/'
PLAYER_URL = BASE_URL + 'player/'


class Yahoo(OAuth2):
    pass


class YahooSession(object):
    pass


yahoo = Yahoo(None, None, from_file='auth.json')

if not yahoo.token_is_valid():
    yahoo.refresh_access_token()


def yahoo_session():
    return yahoo.session


def get_league_id(year=None):
    if year is None:
        year = int(datetime.datetime.now().year)  # current year
    from back_end.lib.db_session import db_session
    db_session = db_session()
    return db_session.query(League.id).filter(League.year == year).one()[0]


def league_url(year=None):
    if year is None:
        year = int(datetime.datetime.now().year)  # current year
    league_id = get_league_id(year)
    return BASE_URL + 'league/{league_id}/'.format(league_id=league_id)


def team_url(team_id, year=None):
    if len(team_id) < 3:

        if year is None:
            year = int(datetime.datetime.now().year)  # current year

        league_id = get_league_id(year)
        team_id = league_id + '.t.' + str(team_id)

    return BASE_URL + 'team/' + team_id + '/'


def save_data(data):
    with open('data.json', 'w') as fp:
        json.dump(data, fp)
