import datetime

from back_end.models.player import Player
from back_end.models.season_stat import SeasonStat
from back_end.yahoo.get_player_game_stats import list_to_dict, chunks
from back_end.yahoo.stats import stat_mapping_dict
from back_end.yahoo.yahoo_session import PLAYER_URL, BASE_URL


def process_player_stats(db_session, player_data):
    stats = player_data['player'][1]['player_stats']['stats']
    player_info = list_to_dict(player_data['player'][0])

    player_id = player_info['player_key']
    year = int(player_data['player'][1]['player_stats']['0']['season'])

    stat_mapping = stat_mapping_dict()
    params = {
        'player_id': player_id,
        'year': year,
    }
    for stat in stats:
        stat_id = stat['stat']['stat_id']
        value = stat['stat']['value']

        if stat_id in stat_mapping and value != '-':
            params[stat_mapping[stat_id]['field']] = value

    instance = db_session.query(SeasonStat).filter(SeasonStat.player_id == player_id, SeasonStat.year == year).first()

    if instance:
        for key, val in params.items():
            setattr(instance, key, val)

    else:
        instance = SeasonStat(**params)
        db_session.merge(instance)

    db_session.commit()


def get_player_season_stats(ys, db_session, player_id, year):
    url = PLAYER_URL + '{player_id}/stats;type=season;season={year}?format=json'.format(player_id=player_id,
                                                                                        year=year)
    response = ys.get(url)
    data = response.json()
    # save_data(data)

    player_data = data['fantasy_content']

    process_player_stats(db_session, player_data)


def get_player_season_stats_group(ys, db_session, player_ids, year):
    if isinstance(player_ids, list):
        player_ids = ','.join(player_ids)

    url = BASE_URL + 'players;player_keys={player_ids}/stats;type=season;season={year}?format=json'.format(
        player_ids=player_ids, year=year)
    response = ys.get(url)
    data = response.json()
    # save_data(data)

    players = data['fantasy_content']['players']
    players.pop('count')

    for key, val in players.items():
        process_player_stats(db_session, val)


def get_all_player_season_stats(ys, db_session, refresh_all=False):
    player_data = db_session.query(Player.id)
    current_year = int(datetime.datetime.now().year)
    years = [current_year - 1 - x for x in range(5)]

    for year in years:

        if not refresh_all:
            ids_complete = db_session.query(SeasonStat.player_id).filter(SeasonStat.year == year).distinct()
            data = player_data.filter(Player.id.notin_(ids_complete)).all()
        else:
            data = player_data.all()

        player_ids = [x[0] for x in data]

        players_chunks = chunks(player_ids, 25)

        for chunk in players_chunks:
            get_player_season_stats_group(ys, db_session, chunk, year)
