import logging

from apistar import Component

from back_end.yahoo.yahoo_session import Yahoo, YahooSession

logger = logging.getLogger(__name__)


class YahooSessionComponent(Component):
    def __init__(self, file_url: str) -> None:
        """
        Configure a new Yahoo Session

        :param url: Auth file location.
        """
        self.file_url = file_url
        # self.yahoo = Yahoo(None, None, from_file=file_url)

        logger.info('Yahoo connection created')
        logger.debug('Auth file used: %s', file_url)

    def resolve(self) -> YahooSession:
        yahoo = Yahoo(None, None, from_file=self.file_url)

        if not yahoo.token_is_valid():
            yahoo.refresh_access_token()

        return yahoo.session
