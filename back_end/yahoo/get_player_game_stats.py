import datetime

from back_end.models.game_schedule import GameSchedule
from back_end.models.game_stat import GameStat
from back_end.yahoo.helpers import list_to_dict, chunks, stats_to_dict
from back_end.yahoo.stats import stat_mapping_dict
from back_end.yahoo.yahoo_session import PLAYER_URL, BASE_URL, league_url


def process_player_stats(db_session, player_data):
    stats = player_data['player'][1]['player_stats']['stats']
    player_info = list_to_dict(player_data['player'][0])

    player_id = player_info['player_key']
    date = player_data['player'][1]['player_stats']['0']['date']

    stat_mapping = stat_mapping_dict()
    params = {
        'player_id': player_id,
        'date': date,
        'last_updated': (datetime.date.today()).strftime('%Y-%m-%d'),
    }
    for stat in stats:
        stat_id = stat['stat']['stat_id']
        value = stat['stat']['value']

        if stat_id in stat_mapping and value != '-':
            params[stat_mapping[stat_id]['field']] = value

    # if player did not play
    if 'gp' not in params:
        return

    instance = db_session.query(GameStat).filter(GameStat.player_id == player_id, GameStat.date == date).first()

    if instance:
        for key, val in params.items():
            setattr(instance, key, val)

    else:
        instance = GameStat(**params)
        db_session.merge(instance)

    db_session.commit()


def get_player_game_stats(ys, db_session, player_id, date):
    url = PLAYER_URL + '{player_id}/stats;type=date;date={date}?format=json'.format(player_id=player_id,
                                                                                    date=date)
    response = ys.get(url)
    data = response.json()
    # save_data(data)

    stats = data['fantasy_content']

    process_player_stats(db_session, stats)


def get_player_game_stats_group(ys, db_session, player_ids, date):
    if isinstance(player_ids, list):
        player_ids = ','.join(player_ids)

    url = BASE_URL + 'players;player_keys={player_ids}/stats;type=date;date={date}?format=json'.format(
        player_ids=player_ids, date=date)
    response = ys.get(url)
    data = response.json()
    # save_data(data)

    players = data['fantasy_content']['players']
    players.pop('count')

    for key, val in players.items():
        process_player_stats(db_session, val)


def get_dates_to_retrieve(db_session):
    today = datetime.date.today()

    # retrieve all nhl games dates
    all_game_dates_str = [x.date for x in db_session.query(GameSchedule.date).distinct().all()]
    all_game_dates = [datetime.datetime.strptime(x, '%Y-%m-%d').date() for x in all_game_dates_str]

    # remove future dates
    all_game_dates = [x for x in all_game_dates if x <= today]

    # get exiting dates
    query = db_session.query(GameStat.date).filter(GameStat.date != GameStat.last_updated).distinct()
    existing_dates_str = [x[0] for x in query.all()]
    existing_dates = [datetime.datetime.strptime(x, '%Y-%m-%d').date() for x in existing_dates_str]

    # remove existing_dates from all_game_dates and convert to date string
    all_game_dates = [x.strftime('%Y-%m-%d') for x in all_game_dates if x not in existing_dates]

    return all_game_dates


def get_player(ys, date, start):
    url = league_url()
    # this url gets ALL players and sorts them by GP(0) for a date(sort_date)
    url += 'players;status=ALL;sort=0;sort_type=date;sort_date={date};start={start}'
    # stats are refined to the same date as above
    url += '/stats;type=date;date={date}?format=json'

    response = ys.get(url.format(date=date, start=start))
    data = response.json()
    # save_data(data)

    players = data['fantasy_content']['league'][1]['players']
    players.pop('count')

    found = True
    player_ids = []

    for key, val in players.items():
        player_data = list_to_dict(val['player'])
        player_profile = list_to_dict(val['player'][0])
        player_id = player_profile['player_key']

        stats = player_data['player_stats']['stats']
        stats = stats_to_dict(stats)
        # 1=player G , 19=goalie win
        if ('1' in stats and stats['1'] == '-') or ('19' in stats and stats['19'] == '-'):
            found = False
        else:
            player_ids.append(player_id)

    return found, player_ids


def get_players_for_date(ys, date):
    step = 0
    again = True
    player_id_list = []
    while again:
        again, player_ids = get_player(ys, date, step)
        player_id_list += player_ids
        step += 25

    return player_id_list


def get_all_player_game_stats(ys, db_session):
    possible_dates = get_dates_to_retrieve(db_session)

    for date in possible_dates:
        players = get_players_for_date(ys, date)
        players_chunks = chunks(players, 25)

        for chunk in players_chunks:
            get_player_game_stats_group(ys, db_session, chunk, date)
