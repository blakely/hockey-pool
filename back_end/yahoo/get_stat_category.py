from back_end.models.stat_category import StatCategory
from back_end.yahoo.yahoo_session import BASE_URL, league_url


def get_stat_category(ys, db_session):
    """
    Get all available stat category.
    """
    url = BASE_URL
    url += 'game/386/stat_categories?format=json'

    response = ys.get(url)
    data = response.json()

    stat_categories = data['fantasy_content']['game'][1]['stat_categories']['stats']

    for cat in stat_categories:
        params = {
            'id': str(cat['stat']['stat_id']),
            'name': cat['stat']['name'],
            'display_name': cat['stat']['display_name']
        }

        instance = StatCategory(**params)
        db_session.merge(instance)

    db_session.commit()


def get_tracked_stat_category(ys, db_session):
    """
    Get stat categories that apply to this league
    """
    url = league_url()
    url += 'settings?format=json'

    response = ys.get(url)
    data = response.json()

    stat_categories = data['fantasy_content']['league'][1]['settings'][0]['stat_categories']['stats']

    for cat in stat_categories:
        instance = db_session.query(StatCategory).filter(StatCategory.id == str(cat['stat']['stat_id'])).one()
        instance.tracked = True

    db_session.commit()
