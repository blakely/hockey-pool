import datetime

from back_end.models.target import Target
from back_end.yahoo.stats import stat_mapping_dict
from back_end.yahoo.yahoo_session import league_url, get_league_id, save_data


def get_target(ys, db_session, year):
    """
    Target's are the score of 1st place in a given category.
    """

    url = league_url(year - 1)
    url += 'standings?format=json'

    response = ys.get(url)
    data = response.json()
    # save_data(data)

    teams = data['fantasy_content']['league'][1]['standings'][0]['teams']
    teams.pop('count', None)

    smd = stat_mapping_dict()

    params = {'league_id': get_league_id(year), }
    for key, val in smd.items():
        if val['tracked']:
            params[val['field']] = None

    for key, val in teams.items():
        team_stats = val['team'][1]['team_stats']['stats']

        for stat in team_stats:
            stat_id = str(stat['stat']['stat_id'])
            value = float(stat['stat']['value'])

            fld = smd[stat_id]['field'] if stat_id in smd and smd[stat_id]['field'] in params else None

            if fld and params[fld] is None:
                params[fld] = value

            elif fld and fld == 'gaa' and value < params[fld]:
                params[fld] = value

            elif fld and fld != 'gaa' and value > params[fld]:
                params[fld] = value

    instance = Target(**params)
    db_session.merge(instance)
    db_session.commit()


def get_current_and_recent_targets(ys, db_session):
    current_year = int(datetime.datetime.now().year)
    for x in range(5):
        get_target(ys, db_session, current_year - x)
