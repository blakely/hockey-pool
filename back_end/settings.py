import os

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
BACKEND_DIR = os.path.join(BASE_DIR, 'back_end')

FRONTEND_DIR = os.path.join(BASE_DIR, 'front_end')
DB_FILE = 'draft.db'
SQLALCHEMY_OPTIONS = {'url': 'sqlite:///{file}'.format(file=DB_FILE)}

DEBUG = False
