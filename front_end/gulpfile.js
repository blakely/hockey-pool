// Include gulp
var gulp = require('gulp');

// Include Our Plugins
// var jshint = require('gulp-jshint');
// var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var html2js = require('gulp-html2js');
// var run = require('gulp-run-command').default;

var RESOURCES = '';
// var PORTAL = 'portal/static/portal/';

var file_list_js = [
    // app.js need to be loaded first
    RESOURCES + 'app/app.js',
    RESOURCES + 'html.js',
    RESOURCES + 'app/**/*.js',
];

var file_list_html = [
    RESOURCES + 'app/**/*.html',
];

// var file_list_scss = [
//     RESOURCES + 'styles/sass/**/*.scss',
// ];

// var file_list_lang = [
//     'locale_front_end/locale-combined.json',
// ];

// Lint Task
// gulp.task('lint', function() {
//     return gulp.src(file_list_js)
//         .pipe(jshint())
//         .pipe(jshint.reporter('default'));
// });

// Compile Our Sass
// gulp.task('sass', function() {
//     return gulp.src(file_list_scss)
//         .pipe(sass({outputStyle: 'compressed'}))
//         .pipe(gulp.dest(PORTAL + 'resources/css'));
// });

// Concatenate HTML templates
// gulp.task('html', function () {
//     return gulp.src(file_list_html)
//         .pipe(htmlToJs({ global: 'window.templates', concat: 'html.js' }))
//         .pipe(gulp.dest(RESOURCES + ''));
// });

gulp.task('html', function () {
    gulp.src(file_list_html)
        .pipe(html2js('html.js', {
            adapter: 'angular',
            base: '.',
            name: 'app'
        }))
        .pipe(gulp.dest(RESOURCES + ''));
});

// Concatenate JS
gulp.task('scripts', function () {
    return gulp.src(file_list_js)
        .pipe(concat('all.js'))
        .pipe(gulp.dest(RESOURCES + ''));
});

// Concatenate & Minify JS
gulp.task('min_scripts', function () {
    return gulp.src(file_list_js)
        .pipe(concat('all.js'))
        .pipe(gulp.dest(RESOURCES + ''))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest(RESOURCES + ''));
});

// Watch Files For Changes
// gulp.task('watch', function() {
//     gulp.watch(file_list_js, ['lint', 'scripts']);
//     // gulp.watch(file_list_scss, ['sass']);
// //    gulp.watch(file_list_lang, ['lang']);
// });

gulp.task('watchlite', function () {
    gulp.watch(file_list_js, ['scripts']);
    gulp.watch(file_list_html, ['html']);
    // gulp.watch(file_list_scss, ['sass']);
    //    gulp.watch(file_list_lang, ['lang']);
});

// gulp.task('lang', run('python manage.py make_json'));

// Default Task
// gulp.task('default', ['lint', 'sass', 'scripts', 'lang', 'watch']);
gulp.task('default', ['html', 'scripts', 'watchlite']);

gulp.task('all', ['lint', 'sass', 'min_scripts', 'lang']);
