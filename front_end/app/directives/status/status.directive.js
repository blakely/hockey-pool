(function () {
    'use strict';

    angular
        .module('app')
        .directive('status', status);

    status.$inject = ['$timeout'];
    function status($timeout) {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            restrict: 'A',
            replace: true,
            scope: {
                status: '=',
            },
            templateUrl: STATIC_ROOT + 'directives/status/status.part.html',
            link: link,
        };
        return directive;

        function link(scope) {
            if (!angular.isDefined(scope.status)) {
                scope.status = 0;
            }

            scope.$watch('status', function (new_val) {
                if (new_val == 2 || new_val == 3) {
                    clear_show();
                }
            });

            function clear_show() {
                $timeout(function () {
                    scope.status = 0;
                }, 1000 * 5);
            }

        }
    }

})();