(function () {
    'use strict';

    angular
        .module('app', [
            'ui.router',

            'ngTouch',
            'ui.grid',
            'ui.grid.selection',
            'ui.grid.cellNav',

            'ngAria',

            // 'ngSanitize',
            // 'swaggerUi',
            // 'ngCookies',
            // 'ui.bootstrap',
            // 'rzModule',
            // 'chart.js',
            // 'pascalprecht.translate',
            // 'angular.filter',
            // 'ngMap',
            // 'ngAnimate',
            // 'mwl.calendar',
            // 'ngFileUpload',
            // 'ngSanitize'
            //'ui.grid',
        ]);

    // ui.router = https://github.com/angular-ui/ui-router
    // ngCookies = Angular package
    // ui.bootstrap = https://angular-ui.github.io/bootstrap/
    // rzModule = https://github.com/angular-slider/angularjs-slider
    // chart.js = https://jtblin.github.io/angular-chart.js/
    // pascalprecht.translate = https://github.com/angular-translate/angular-translate
    // angular.filter = https://github.com/a8m/angular-filter
    // ngMap = http://ngmap.github.io
    // mwl.calendar = https://github.com/mattlewis92/angular-bootstrap-calendar
    // ui.grid = https://github.com/angular-ui/ui-grid


    // ngFileUpload = https://github.com/danialfarid/ng-file-upload  REMOVED FOR NOW

})();


