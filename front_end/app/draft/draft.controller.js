(function () {
    'use strict';

    angular
        .module('app')
        .controller('DraftController', DraftController);

    DraftController.$inject = ['DataService', 'uiGridConstants'];
    function DraftController(DataService, uiGridConstants) {
        var vm = this;
        if (DEBUG) {
            console.log(vm);
        }

        vm.manual_team_id = DataService.manual_team_id;

        vm.set_visibility = DataService.set_visibility;
        vm.reset_visibility = DataService.reset_visibility;

        vm.team = DataService.team_data;
        vm.data = DataService.player_data;
        vm.draft_resutls = DataService.draft_resutls;
        vm.selected_player = {};

        vm.skater_data_grid = {
            data: DataService.skater_data,
            enableFiltering: true,
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            enableColumnMenus: false,
            multiSelect: false,
            modifierKeysToMultiSelect: false,
            noUnselect: true,
            enableHorizontalScrollbar: 0,
            useExternalSorting: false,
            onRegisterApi: function (gridApi) {
                gridApi.selection.on.rowSelectionChanged(null, on_skater_change);
                vm.grid_api_skater = gridApi;
            },

            columnDefs: [
                { name: 'Name', field: 'name', width: 150 },
                { name: 'Pos', field: 'pos', headerCellClass: 'header_text' },
                { name: 'Team', field: 'team', headerCellClass: 'header_text' },

                { name: 'GP', displayName: 'GP', field: 'gp', type: 'number', cellTemplate: field_template('gp'), enableFiltering: true, headerCellClass: 'bg-gp header_text', sortDirectionCycle: [uiGridConstants.DESC], filters: [{ condition: uiGridConstants.filter.GREATER_THAN, placeholder: 'gt' }] },
                { name: 'G', displayName: 'G', field: 'g', type: 'number', cellTemplate: stat_template('g'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'A', displayName: 'A', field: 'a', type: 'number', cellTemplate: stat_template('a'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: '+/-', displayName: '+/-', field: 'plus_minus', type: 'number', cellTemplate: stat_template('plus_minus'), enableFiltering: false, headerCellClass: 'bg-plus_minus header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PIM', displayName: 'PIM', field: 'pim', type: 'number', cellTemplate: stat_template('pim'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PPP', displayName: 'PPP', field: 'ppp', type: 'number', cellTemplate: stat_template('ppp'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'SHP', displayName: 'SHP', field: 'shp', type: 'number', cellTemplate: stat_template('shp'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GWG', displayName: 'GWG', field: 'gwg', type: 'number', cellTemplate: stat_template('gwg'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'HIT', displayName: 'HIT', field: 'hit', type: 'number', cellTemplate: stat_template('hit'), enableFiltering: false, headerCellClass: 'bg-hit header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'BLK', displayName: 'BLK', field: 'blk', type: 'number', cellTemplate: stat_template('blk'), enableFiltering: false, headerCellClass: 'bg-blk header_text', sortDirectionCycle: [uiGridConstants.DESC] },

                { name: 'TS', displayName: 'TS', field: 'total', type: 'number', cellTemplate: field_template('total'), enableFiltering: false, headerCellClass: 'header_text', defaultSort: { direction: uiGridConstants.DESC, priority: 0 }, sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'OS', displayName: 'OS', field: 'ofence', type: 'number', cellTemplate: field_template('ofence'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'DS', displayName: 'DS', field: 'defence', type: 'number', cellTemplate: field_template('defence'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PS', displayName: 'PS', field: 'points', type: 'number', cellTemplate: field_template('points'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
            ]
        };

        vm.goalie_data_grid = {
            data: DataService.goalie_data,
            enableFiltering: true,
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            enableColumnMenus: false,
            multiSelect: false,
            modifierKeysToMultiSelect: false,
            noUnselect: true,
            enableHorizontalScrollbar: 0,
            onRegisterApi: function (gridApi) {
                gridApi.selection.on.rowSelectionChanged(null, on_goalie_change);
                vm.grid_api_goalie = gridApi;
            },
            columnDefs: [

                { name: 'Name', field: 'name', width: 150 },
                { name: 'Pos', field: 'pos', width: 40, enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'Team', field: 'team', enableFiltering: false, headerCellClass: 'header_text' },

                { name: 'GP', displayName: 'GP', field: 'gp', type: 'number', width: 35, enableFiltering: false, cellTemplate: field_template('gp'), headerCellClass: 'bg-gp header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GS', displayName: 'GS', field: 'gs', type: 'number', width: 40, enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'W', displayName: 'W', field: 'w', type: 'number', width: 40, enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GAA', displayName: 'GAA', field: 'gaa', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.ASC] },
                { name: 'SV%', displayName: 'SV%', field: 'sv_p', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'SHO', displayName: 'SHO', field: 'sho', type: 'number', width: 45, enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'W%', displayName: 'W%', field: 'w_p', type: 'number', width: 60, cellTemplate: field_template('w_p'), enableFiltering: false, headerCellClass: 'header_text', defaultSort: { direction: uiGridConstants.DESC } },
            ]
        };

        vm.player_data_grid = {
            data: [],
            enableFiltering: false,
            enableSorting: false,
            enableColumnMenus: false,
            enableHorizontalScrollbar: 0,
            enableVerticalScrollbar: 0,
            onRegisterApi: function (gridApi) {
                vm.grid_api = gridApi;
            },
            columnDefs: []
        };

        vm.columns = {
            skater: [
                { name: 'Year', field: 'year', cellClass: 'header_text', width: 45, sort: { direction: uiGridConstants.DESC, priority: 0, }, headerCellClass: 'bg-gp header_text' },
                { name: 'GP', displayName: 'GP', field: 'gp', width: 35, cellTemplate: field_template('gp'), enableFiltering: false, headerCellClass: 'bg-gp header_text' },
                { name: 'G', displayName: 'G', field: 'g', cellTemplate: stat_template('g'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'A', displayName: 'A', field: 'a', cellTemplate: stat_template('a'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: '+/-', displayName: '+/-', field: 'plus_minus', cellTemplate: stat_template('plus_minus'), enableFiltering: false, headerCellClass: 'bg-plus_minus header_text' },
                { name: 'PIM', displayName: 'PIM', field: 'pim', cellTemplate: stat_template('pim'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'PPP', displayName: 'PPP', field: 'ppp', cellTemplate: stat_template('ppp'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'SHP', displayName: 'SHP', field: 'shp', cellTemplate: stat_template('shp'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'GWG', displayName: 'GWG', field: 'gwg', cellTemplate: stat_template('gwg'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'HIT', displayName: 'HIT', field: 'hit', cellTemplate: stat_template('hit'), enableFiltering: false, headerCellClass: 'bg-hit header_text' },
                { name: 'BLK', displayName: 'BLK', field: 'blk', cellTemplate: stat_template('blk'), enableFiltering: false, headerCellClass: 'bg-blk header_text' },

            ],
            goalie: [
                { name: 'Year', field: 'year', cellClass: 'header_text', width: 45, sort: { direction: uiGridConstants.DESC, priority: 0, }, headerCellClass: 'bg-gp header_text' },
                { name: 'GP', displayName: 'GP', field: 'gp', width: 35, cellTemplate: field_template('gp'), enableFiltering: false, headerCellClass: 'bg-gp header_text' },
                { name: 'GS', displayName: 'GS', field: 'gs', enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'W', displayName: 'W', field: 'w', enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'GAA', displayName: 'GAA', field: 'gaa', enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'SV%', displayName: 'SV%', field: 'sv_p', enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'SHO', displayName: 'SHO', field: 'sho', enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'W%', displayName: 'W%', field: 'w_avg', cellTemplate: field_template('w_avg'), enableFiltering: false, headerCellClass: 'header_text' },
            ]
        };

        vm.team_data_grid = {
            data: DataService.team_data,
            enableFiltering: false,
            enableSorting: true,
            enableColumnMenus: false,
            enableHorizontalScrollbar: 0,
            enableVerticalScrollbar: 0,
            onRegisterApi: function (gridApi) {
                vm.grid_api_team = gridApi;
            },
            columnDefs: [
                { name: 'Name', field: 'nick_name', width: 180 },

                { name: 'GP', displayName: 'GP', field: 'gp_skater', type: 'number', width: 50, enableFiltering: false, cellTemplate: field_template('gp_skater'), headerCellClass: 'bg-gp header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'G', displayName: 'G', field: 'g', type: 'number', enableFiltering: false, cellTemplate: rank_template('g'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'A', displayName: 'A', field: 'a', type: 'number', enableFiltering: false, cellTemplate: rank_template('a'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: '+/-', displayName: '+/-', field: 'plus_minus', type: 'number', enableFiltering: false, cellTemplate: rank_template('plus_minus'), headerCellClass: 'bg-plus_minus header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PIM', displayName: 'PIM', field: 'pim', type: 'number', enableFiltering: false, cellTemplate: rank_template('pim'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PPP', displayName: 'PPP', field: 'ppp', type: 'number', enableFiltering: false, cellTemplate: rank_template('ppp'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'SHP', displayName: 'SHP', field: 'shp', type: 'number', enableFiltering: false, cellTemplate: rank_template('shp'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GWG', displayName: 'GWG', field: 'gwg', type: 'number', enableFiltering: false, cellTemplate: rank_template('gwg'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'HIT', displayName: 'HIT', field: 'hit', type: 'number', enableFiltering: false, cellTemplate: rank_template('hit'), headerCellClass: 'bg-hit header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'BLK', displayName: 'BLK', field: 'blk', type: 'number', enableFiltering: false, cellTemplate: rank_template('blk'), headerCellClass: 'bg-blk header_text', sortDirectionCycle: [uiGridConstants.DESC] },

                { name: 'GP ', displayName: 'GP', field: 'gp_goalie', type: 'number', width: 50, enableFiltering: false, cellTemplate: field_template('gp_goalie'), headerCellClass: 'bg-gp header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GS', displayName: 'GS', field: 'gs', type: 'number', enableFiltering: false, cellTemplate: rank_template('gs'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'W', displayName: 'W', field: 'w', type: 'number', enableFiltering: false, cellTemplate: rank_template('w'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GAA', displayName: 'GAA', field: 'gaa', type: 'number', enableFiltering: false, cellTemplate: rank_template('gaa'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.ASC] },
                { name: 'SV%', displayName: 'SV%', field: 'sv_p', type: 'number', enableFiltering: false, cellTemplate: rank_template('sv_p'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'SHO', displayName: 'SHO', field: 'sho', type: 'number', enableFiltering: false, cellTemplate: rank_template('sho'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
            ]
        };

        vm.target_data_grid = {
            data: [DataService.targets],
            enableFiltering: false,
            enableSorting: true,
            enableColumnMenus: false,
            enableHorizontalScrollbar: 0,
            enableVerticalScrollbar: 0,
            showHeader: false,
            onRegisterApi: function (gridApi) {
                vm.grid_api_target = gridApi;
            },
            columnDefs: [
                { name: 'Targets', field: 'name', width: 180 },

                { name: 'GP', field: 'gp_skater', type: 'number', width: 50, enableFiltering: false, cellTemplate: field_template('gp_skater'), },
                { name: 'G', field: 'g', type: 'number', enableFiltering: false, },
                { name: 'A', field: 'a', type: 'number', enableFiltering: false, },
                { name: '+/-', field: 'plus_minus', type: 'number', enableFiltering: false, cellTemplate: field_template('plus_minus'), sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PIM', field: 'pim', type: 'number', enableFiltering: false, },
                { name: 'PPP', field: 'ppp', type: 'number', enableFiltering: false, },
                { name: 'SHP', field: 'shp', type: 'number', enableFiltering: false, },
                { name: 'GWG', field: 'gwg', type: 'number', enableFiltering: false, },
                { name: 'HIT', field: 'hit', type: 'number', enableFiltering: false, cellTemplate: field_template('hit'), },
                { name: 'BLK', field: 'blk', type: 'number', enableFiltering: false, cellTemplate: field_template('blk'), },

                { name: 'GP ', field: 'gp_goalie', type: 'number', width: 50, enableFiltering: false, cellTemplate: field_template('gp_goalie'), },
                { name: 'GS', field: 'gs', type: 'number', enableFiltering: false, },
                { name: 'W', field: 'w', type: 'number', enableFiltering: false, },
                { name: 'GAA', field: 'gaa', type: 'number', enableFiltering: false, },
                { name: 'SV%', field: 'sv_p', type: 'number', enableFiltering: false, },
                { name: 'SHO', field: 'sho', type: 'number', enableFiltering: false, },
            ]
        };

        function field_template(field) {
            return '<div class="ui-grid-cell-contents bg-' + field + '" ng-class="{\'bg-good\':row.entity.' + field + '_trend==1, \'bg-bad\':row.entity.' + field + '_trend==3}" >{{ row.entity.' + field + ' }}</div>';
        }

        function stat_template(field) {
            return '<p class="ui-grid-cell-contents ui-grid-cell-contents-compact bg-' + field + '" ng-class="{\'bg-good\':row.entity.' + field + '_trend==1, \'bg-bad\':row.entity.' + field + '_trend==3}" >' +
                '       <span style="font-size: 90%;">{{ row.entity.' + field + ' }}</span><br>' +
                '       <span style="font-size: 60%;">{{ row.entity.' + field + '_weight | number:4 }}</span>' +
                '   </p>';
        }

        function rank_template(field) {
            return '<p class="ui-grid-cell-contents ui-grid-cell-contents-compact bg-' + field + '" >' +
                '       <span style="font-size: 90%;">{{ row.entity.' + field + ' }}</span><br>' +
                '       <span style="font-size: 60%;">rnk:{{ row.entity.' + field + '_rank }} avg:<span class="text-danger">{{ row.entity.' + field + '_avg_rank }}</span></span>' +
                '   </p>';
        }

        function on_skater_change(row) {
            on_row_selection_change(row, 'skater');
        }
        function on_goalie_change(row) {
            on_row_selection_change(row, 'goalie');
        }

        function on_row_selection_change(row, type) {
            var id = row.entity.id;

            angular.forEach(vm.data, function (item) {
                if (item.id == id) {
                    var stats = angular.copy(item.season_stats);
                    stats.pop();// remove trends entry

                    vm.player_data_grid.data = stats;
                    vm.player_data_grid.columnDefs = vm.columns[type];
                    vm.grid_api.core.notifyDataChange(uiGridConstants.dataChange.ALL);

                    vm.selected_player = angular.copy(item);// binding must be broken
                }
            });
        }

        function remove_player(data) {
            if ('id' in data) {
                DataService.set_visibility(data.id, false).then(
                    function (response) {
                        var pos = 'skater';
                        var player = vm.data[data.id];

                        player.visible = false;
                        if (player.pos == 'G') { pos = 'goalie'; }

                        angular.forEach(DataService[pos + '_data'], function (item, index) {
                            if (item.id == response.data.player_id) {
                                DataService[pos + '_data'].splice(index, 1);
                                vm['grid_api_' + pos].core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                            }
                        });

                        enpty_dict(vm.selected_player);
                        enpty_dict(vm.player_data_grid.data);
                        vm.grid_api.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                    }
                );
            }
        }
        vm.remove_player = remove_player;

        function reset_players() {
            DataService.reset_visibility().then(
                function () {
                    angular.forEach(vm.data, function (item) {
                        item.status.visible = true;
                        item.status.taken = false;
                    });
                    DataService.populate_players_for_tables();
                    vm.grid_api_skater.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                    vm.grid_api_goalie.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                }
            );
        }
        vm.reset_players = reset_players;

        function refresh_draft_results() {

            if (vm.bypass) {
                DataService.get_draft_results().then(
                    function () {
                        vm.grid_api_skater.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                        vm.grid_api_goalie.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                        vm.grid_api_team.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                    }
                );
            } else {
                DataService.refresh_and_get_draft_results().then(
                    function () {
                        vm.grid_api_skater.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                        vm.grid_api_goalie.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                        vm.grid_api_team.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                    }
                );
            }




        }
        vm.refresh_draft_results = refresh_draft_results;

        function enpty_dict(dict) {
            for (var prop in dict) {
                if (dict.hasOwnProperty(prop)) {
                    delete dict[prop];
                }
            }
        }

        init();

        ////////////////

        function init() { }
    }
})();