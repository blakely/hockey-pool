(function () {
    'use strict';

    angular
        .module('app')
        .config(rootRouter);

    rootRouter.$inject = ['$stateProvider', '$urlRouterProvider'];
    function rootRouter($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/home');
        $urlRouterProvider.when('', '/home');

        $stateProvider

            .state('home', {
                url: '/home',
                templateUrl: STATIC_ROOT + 'home/home.view.html',
                controller: 'HomeController',
                controllerAs: 'vm',
                // resolve: {
                // Get Token
                //                     Token: ['AuthService', function(AuthService) {
                // //                        AuthService.ValidateToken();
                //                         AuthService.SetTokenRefreshTimer();
                //                         return AuthService.RefreshToken();
                //                     }],
                //                     // Get user information
                //                     User: ['Token', 'UserService',function(Token, UserService) {
                //                         return UserService.GetMyInformation();
                //                     }],
                //                     // Set User Settings
                //                     Settings: ['User', 'UserService', function(User, UserService) {
                //                         return UserService.SetDefaultSettings();
                //                     }]
                // }
            })

            .state('draft', {
                url: '/draft',
                // parent: 'customer_list',
                templateUrl: STATIC_ROOT + 'draft/draft.view.html',
                controller: 'DraftController',
                controllerAs: 'vm',
            })

            .state('hot_players', {
                url: '/hot_players',
                // parent: 'nav',
                templateUrl: STATIC_ROOT + 'hot_players/hot_players.view.html',
                controller: 'HotPlayersController',
                controllerAs: 'vm'
            })

        ;

    }
})();
