(function () {
    'use strict';

    angular
        .module('app')
        .controller('HotPlayersController', HotPlayersController);

    HotPlayersController.$inject = ['DataService', 'uiGridConstants'];
    function HotPlayersController(DataService, uiGridConstants) {
        var vm = this;
        if (DEBUG) {
            console.log(vm);
        }

        vm.hot_streak_gp = 4;

        function refresh_game_stats() {
            show('refresh_game_stats', 1);
            DataService.refresh_game_stats().then(
                function () {
                    show('refresh_game_stats', 2);
                },
                function () {
                    show('refresh_game_stats', 3);
                }
            );
        }
        vm.refresh_game_stats = refresh_game_stats;

        vm.hot_streak_data = [];
        function get_hot_streaks() {
            var game_count = vm.hot_streak_gp;
            show('refresh_hot_streak', 1);

            DataService.get_hot_streaks(game_count).then(
                function (response) {
                    vm.hot_streak_data.splice(0, vm.hot_streak_data.length);
                    // console.log(response);
                    angular.forEach(response.data, function (val, key) {
                        val.name = get_player_info(key, 'name');
                        val.team = get_player_info(key, 'team');
                        val.pos = get_player_info(key, 'pos');
                        val.owner_name = get_player_info(key, 'owner_name');
                        if (val.pos != 'G') {
                            this.push(val);
                        }
                    }, vm.hot_streak_data);
                    show('refresh_hot_streak', 2);
                },
                function () {
                    show('refresh_hot_streak', 3);
                }
            );
        }
        vm.get_hot_streaks = get_hot_streaks;


        function get_player_info(player_id, field) {
            return DataService.player_data[player_id][field];
        }




        /////////////////////// tables
        vm.hot_streak_grid = {
            data: vm.hot_streak_data,
            enableFiltering: true,
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            enableColumnMenus: false,
            multiSelect: false,
            modifierKeysToMultiSelect: false,
            noUnselect: true,
            enableHorizontalScrollbar: 0,
            useExternalSorting: false,
            // onRegisterApi: function (gridApi) {
            //     // gridApi.selection.on.rowSelectionChanged(null, on_skater_change);
            //     // vm.grid_hot_streak = gridApi;
            // },

            columnDefs: [
                index_row(3),
                { name: 'Name', field: 'name', width: 150 },
                { name: 'Pos', field: 'pos', headerCellClass: 'header_text' },
                { name: 'Team', field: 'team', headerCellClass: 'header_text' },
                { name: 'Owner', field: 'owner_name', headerCellClass: 'header_text', width: 150 },

                { name: 'GP', displayName: 'GP', field: 'gp', type: 'number', cellTemplate: stat_template('gp'), enableFiltering: true, headerCellClass: 'bg-gp header_text', sortDirectionCycle: [uiGridConstants.DESC], },
                { name: 'G', displayName: 'G', field: 'g', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'A', displayName: 'A', field: 'a', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: '+/-', displayName: '+/-', field: 'plus_minus', type: 'number', cellTemplate: stat_template('plus_minus'), enableFiltering: false, headerCellClass: 'bg-plus_minus header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PIM', displayName: 'PIM', field: 'pim', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PPP', displayName: 'PPP', field: 'ppp', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'SHP', displayName: 'SHP', field: 'shp', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GWG', displayName: 'GWG', field: 'gwg', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'HIT', displayName: 'HIT', field: 'hit', type: 'number', cellTemplate: stat_template('hit'), enableFiltering: false, headerCellClass: 'bg-hit header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'BLK', displayName: 'BLK', field: 'blk', type: 'number', cellTemplate: stat_template('blk'), enableFiltering: false, headerCellClass: 'bg-blk header_text', sortDirectionCycle: [uiGridConstants.DESC] },

            ]
        };



        init();

        ////////////////

        function init() {
            vm.show = {};
        }

        function stat_template(field) {
            return '<p class="ui-grid-cell-contents ui-grid-cell-contents-compact bg-' + field + '" ng-class="{\'bg-good\':row.entity.' + field + '_trend==1, \'bg-bad\':row.entity.' + field + '_trend==3}" >' +
                '       <span style="font-size: 90%;">{{ row.entity.' + field + ' }}</span><br>' +
                '       <span style="font-size: 60%;">{{ row.entity.' + field + '_weight | number:4 }}</span>' +
                '   </p>';
        }

        function index_row(number) {
            return { name: 'index', field: 'index', displayName: '', width: 13 * number, enableFiltering: false, cellTemplate: '<div class="ui-grid-cell-contents">{{grid.renderContainers.body.visibleRowCache.indexOf(row)+1}}</div>' };
        }

        function show(item, val) {
            vm.show[item] = val;
        }





    }
})();

// service.draft_resutls.splice(0, service.draft_resutls.length);
//                         angular.forEach(response.data, function (item) {
//                             this.push(item);
//                         }, service.draft_resutls);