(function () {
    'use strict';

    angular
        .module('app')
        .controller('RootController', RootController);

    RootController.$inject = ['DataService'];
    function RootController(DataService) {
        var vm = this;
        if (DEBUG) {
            console.log(vm);
        }

        vm.loading = DataService.loading;


        init();

        ////////////////

        function init() { }
    }
})();


(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    // HomeController.$inject = ['DataService'];
    function HomeController() {
        var vm = this;
        if (DEBUG) {
            console.log(vm);
        }

        init();

        ////////////////

        function init() { }
    }
})();