(function () {
    'use strict';

    angular
        .module('app')
        .factory('DataService', DataService);

    DataService.$inject = ['$http', '$q'];
    function DataService($http, $q) {
        var service = {
            loading: { loading: true },

            reset_visibility: reset_visibility,
            set_visibility: set_visibility,
            populate_players_for_tables: populate_players_for_tables,

            get_draft_results: get_draft_results,
            refresh_and_get_draft_results: refresh_and_get_draft_results,

            player_data: {},
            team_player_data: {},
            skater_data: [],
            goalie_data: [],
            draft_resutls: [],
            teams: {},
            team_data: [],
            targets: { // backup targets
                g: 315,
                a: 501,
                plus_minus: 90,
                pim: 660,
                ppp: 273,
                shp: 17,
                gwg: 57,
                hit: 1600,
                blk: 980,
                gs: 131,
                w: 84,
                gaa: 2.11,
                sv_p: 0.925,
                sho: 14,
            },

            manual_team_id: '386.l.2131.t.3',

            // game stats section
            refresh_game_stats: refresh_game_stats,
            get_hot_streaks: get_hot_streaks,
        };

        init();
        return service;

        function init() {
            // console.log(service);            
            get_team_data();
            get_team_players().then(
                function () {
                    get_targets().then(
                        function () {
                            get_player_data().then(// requires target & team_players
                                function () {
                                    service.loading.loading = false;
                                }
                            );
                        }
                    );
                }
            );

        }

        ////////////////
        function get_player_data() {
            var url = API_ROOT + 'player/';
            return http('GET', url).then(
                function (response) {
                    angular.forEach(response.data, function (item) {
                        this[item.id] = item;
                    }, service.player_data);
                    process_data(service.player_data);
                    return response;
                }
            );
        }

        function get_team_players() {
            var url = API_ROOT + 'team_players/';
            return http('GET', url).then(
                function (response) {
                    angular.forEach(response.data, function (item) {
                        this[item.player_id] = item.team_id;
                    }, service.team_player_data);
                    return response;
                }
            );
        }

        function get_team_data() {
            var url = API_ROOT + 'team/';
            http('GET', url).then(
                function (response) {
                    angular.forEach(response.data, function (item) {
                        this[item.id] = item;
                    }, service.teams);
                }
            );

        }

        function get_draft_results() {
            var results = $q.defer();

            var request = function () {
                var url = API_ROOT + 'draft_results/';
                http('GET', url).then(
                    function (response) {
                        service.draft_resutls.splice(0, service.draft_resutls.length);
                        angular.forEach(response.data, function (item) {
                            this.push(item);
                        }, service.draft_resutls);
                        players_update_taken();
                        results.resolve(true);
                    }
                );

            };
            request();
            return results.promise;
        }

        function refresh_and_get_draft_results() {
            var results = $q.defer();

            var request = function () {
                var url = API_ROOT + 'draft_results/';
                http('PATCH', url).then(
                    function (response) {
                        if (response.status == 200) {
                            get_draft_results().then(
                                function () {
                                    results.resolve(true);
                                }
                            );
                        }
                    }
                );

            };
            request();
            return results.promise;
        }

        function get_targets() {
            var url = API_ROOT + 'target/';
            return http('GET', url).then(
                function (response) {
                    var current_year = (new Date()).getFullYear();

                    angular.forEach(response.data, function (target) {
                        if (target.league.year == current_year) {
                            angular.forEach(target, function (val, key) {

                                this[key] = val;
                            }, service.targets);
                        }
                    });
                    service.targets.name = 'Targets';

                    return response;
                }
            );

        }

        function players_update_taken() {
            clear_mine();
            if (Object.keys(service.player_data).length > 0) {
                angular.forEach(service.draft_resutls, function (item) {
                    if (item.player_id && item.player_id in service.player_data) {
                        service.player_data[item.player_id].status.taken = true;
                        if (item.team_id == service.manual_team_id) {
                            service.player_data[item.player_id].mine = true;
                        }
                    }
                });
                // compiling values for table
                populate_players_for_tables();
                compile_teams_results();
            }
        }

        function clear_mine() {
            angular.forEach(service.player_data, function (item) {
                item.mine = false;
            });

        }

        function process_data(player_data) {
            //////////////////////////////////////////////
            // adding a bunch of calculated fields to player season stats
            //////////////////////////////////////////////
            var year = (new Date()).getFullYear() - 1;
            var stat_cols = ['g', 'a', 'plus_minus', 'pim', 'ppp', 'shp', 'gwg', 'hit', 'blk', 'w'];
            var trend_cols = stat_cols.concat(['total', 'ofence', 'defence', 'points']);
            var weight_group = {
                'total': ['g', 'a', 'plus_minus', 'pim', 'ppp', 'shp', 'gwg', 'hit', 'blk'],
                'ofence': ['g', 'a', 'plus_minus', 'ppp', 'gwg'],
                'defence': ['plus_minus', 'pim', 'shp', 'hit', 'blk'],
                'points': ['g', 'a', 'ppp'],
            };
            angular.forEach(player_data, function (row) {
                // adding values to row
                row.owner_id = service.team_player_data[row.id];// not used for draft but for tracking
                row.owner_name = row.owner_id ? service.teams[row.owner_id].nick_name : '';

                row.status.taken = false;// prep for draft_results
                row.pos = make_pos(row);
                angular.forEach(row.season_stats, function (stat) {
                    stat.toi = adjust_toi(stat.toi);// toi str to number
                    // set avg and weight
                    angular.forEach(stat_cols, function (item) {
                        if (stat[item] == null) { stat[item] = 0; }
                        stat[item + '_avg'] = stat.gp == 0 ? 0 : stat[item] / stat.gp;
                        stat[item + '_weight'] = stat[item + '_avg'] / (service.targets[item] / 1000);
                        stat[item + '_avg'] = Math.round(stat[item + '_avg'] * 1000) / 1000;
                    });

                    // set weight groups
                    angular.forEach(weight_group, function (val, key) {
                        stat[key] = 0;
                        angular.forEach(val, function (item) {
                            stat[key] += stat[item] == 0 ? 0 : stat[item + '_weight'];
                        });
                        stat[key] = Math.round(stat[key] * 1000) / 1000;
                        stat[key + '_weight'] = stat[key];
                    });

                });

                // add trend values based on last 3 years
                var trends = { year: 'trend' };
                angular.forEach(trend_cols, function (item) {
                    var y1 = get_stat(row, year, item + '_weight');
                    var y2 = get_stat(row, year - 1, item + '_weight');
                    var y3 = get_stat(row, year - 2, item + '_weight');
                    if (y1 > y2 && y2 > y3) {
                        this[item] = 1;//good
                    }
                    else if (y1 > y2 && y2 < y3) {
                        this[item] = 2;//medium
                    }
                    else if (y1 < y2 && y2 < y3) {
                        this[item] = 3;//bad
                    }
                    else {
                        this[item] = 0;//not worth mentioning
                    }
                }, trends);
                row.season_stats.push(trends);

            });
            // compiling values for table
            populate_players_for_tables();
        }

        function populate_players_for_tables() {
            var year = (new Date()).getFullYear() - 1;
            var stat_cols = ['g', 'a', 'plus_minus', 'pim', 'ppp', 'shp', 'gwg', 'hit', 'blk', 'w'];
            var trend_cols = stat_cols.concat(['total', 'ofence', 'defence', 'points']);

            service.skater_data.splice(0, service.skater_data.length);
            service.goalie_data.splice(0, service.goalie_data.length);

            angular.forEach(service.player_data, function (row, key) {
                if (row.status.visible && !row.status.taken) {
                    var d = {
                        id: key,
                        name: row.name,
                        pos: row.pos,
                        team: row.team,
                        mine: row.mine,
                        gp: get_stat(row, year, 'gp'),
                    };

                    if (row.pos_g) {// if goalie
                        d.gs = get_stat(row, year, 'gs');
                        d.w = get_stat(row, year, 'w');
                        d.gaa = get_stat(row, year, 'gaa');
                        d.sv_p = get_stat(row, year, 'sv_p');
                        d.sho = get_stat(row, year, 'sho');
                        d.w_p = get_stat(row, year, 'w_avg');
                        d.w_p = Math.round(d.w_p * 1000) / 1000;

                        d.w_p_trend = get_stat(row, 'trend', 'w');

                        service.goalie_data.push(d);

                    } else {// if skater
                        angular.forEach(trend_cols, function (item) {
                            this[item] = get_stat(row, year, item);
                            this[item + '_weight'] = get_stat(row, year, item + '_weight');
                            this[item + '_trend'] = get_stat(row, 'trend', item);
                        }, d);

                        service.skater_data.push(d);
                    }
                }
            });
        }

        function compile_teams_results() {
            var year = (new Date()).getFullYear() - 1;
            var sum_cols = ['g', 'a', 'plus_minus', 'pim', 'ppp', 'shp', 'gwg', 'hit', 'blk', 'gs', 'w', 'toi', 'ga', 'sa', 'sv', 'sho'];
            var rank_cols = sum_cols.concat(['gaa', 'sv_p']);

            // set init values
            angular.forEach(service.teams, function (team) {
                angular.forEach(sum_cols, function (item) {
                    this[item] = 0;
                }, team);
                team.gp_skater = 0;
                team.gp_goalie = 0;
            });

            // compile stats
            angular.forEach(service.draft_resutls, function (item) {
                if (item.player_id in service.player_data) {
                    var team = service.teams[item.team_id];// for testing add .replace('old_id', 'new_id')
                    var player = service.player_data[item.player_id];
                    var player_stat;
                    angular.forEach(player.season_stats, function (stat) {
                        if (stat.year == year) { player_stat = stat; }
                    });

                    if (player.pos == 'G') {
                        team.gp_goalie += player_stat.gp;
                    } else {
                        team.gp_skater += player_stat.gp;
                    }
                    angular.forEach(sum_cols, function (col) {
                        team[col] += player_stat[col];
                        team[col + '_avg'] = team[col] / (player.pos == 'G' ? team.gp_goalie : team.gp_skater);// set average for ranking
                    });

                }
            });

            // add gaa and sv_p
            angular.forEach(service.teams, function (team) {
                team.gaa = Math.round((team.ga / (team.toi / 60)) * 100) / 100;
                team.sv_p = Math.round((team.sv / team.sa) * 1000) / 1000;
                team.gaa_avg = team.gaa;
                team.sv_p_avg = team.sv_p;
            });

            // add ranking
            var ranks = {};
            angular.forEach(rank_cols, function (field) {
                ranks[field] = [];
                ranks[field + '_avg'] = [];

                angular.forEach(service.teams, function (team) {
                    this[field].push(team[field]);
                    this[field + '_avg'].push(team[field + '_avg']);
                }, ranks);
            });

            var sorted_ranks = {};
            angular.forEach(rank_cols, function (field) {
                if (field == 'gaa') {
                    this[field] = ranks[field].slice().sort(function (a, b) { return a - b; });
                    this[field + '_avg'] = ranks[field + '_avg'].slice().sort(function (a, b) { return a - b; });
                } else {
                    this[field] = ranks[field].slice().sort(function (a, b) { return b - a; });
                    this[field + '_avg'] = ranks[field + '_avg'].slice().sort(function (a, b) { return b - a; });
                }
            }, sorted_ranks);

            angular.forEach(rank_cols, function (field) {
                angular.forEach(service.teams, function (team) {
                    team[field + '_rank'] = sorted_ranks[field].indexOf(team[field]) + 1;
                    team[field + '_avg_rank'] = sorted_ranks[field + '_avg'].indexOf(team[field + '_avg']) + 1;
                });
            });

            //set team_data
            service.team_data.splice(0, service.team_data.length);
            angular.forEach(service.teams, function (team) {
                this.push(team);
            }, service.team_data);
        }

        function make_pos(row) {
            var str = '';
            if (row.pos_c) { str += ',C'; }
            if (row.pos_w) { str += ',W'; }
            if (row.pos_d) { str += ',D'; }
            if (row.pos_g) { str += ',G'; }
            return str.substring(1);
        }

        function adjust_toi(toi) {
            // take toi str, return toi integer
            if (toi) {
                var data = toi.split(':');
                data[0] = parseInt(data[0].replace(',', ''));
                data[1] = parseFloat(data[1] / 60);
                return data[0] + data[1];

            } else {
                return null;
            }
        }

        function get_stat(row, year, stat_name) {
            for (var i in row.season_stats) {
                if (row.season_stats[i].year == year) {
                    return row.season_stats[i][stat_name];
                }
            }
        }

        function set_visibility(player_id, visible) {
            var url = API_ROOT + 'player/' + player_id + '/status/';
            var data = {
                visible: visible
            };

            return http('PUT', url, data);
        }

        function reset_visibility() {
            var url = API_ROOT + 'status_reset/';
            return http('GET', url);
        }

        function refresh_game_stats() {
            var url = API_ROOT + 'game_stats/';
            return http('PATCH', url);
        }

        function get_hot_streaks(game_count) {
            var url = API_ROOT + 'reports/hot_streak/';
            var params = { game_count: game_count };
            return http('POST', url, params);
        }


        function http(method, url, data, params) {
            var results = $q.defer();
            var request = function () {
                $http({
                    method: method,
                    url: url,
                    data: data,
                    params: params,
                }).then(
                    function (response) {
                        results.resolve(response);
                    }, function (error) {
                        console.error(error);
                        results.reject(error);
                    }
                );
            };
            request();
            return results.promise;
        }
    }
})();

