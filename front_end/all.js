(function () {
    'use strict';

    angular
        .module('app', [
            'ui.router',

            'ngTouch',
            'ui.grid',
            'ui.grid.selection',
            'ui.grid.cellNav',

            'ngAria',

            // 'ngSanitize',
            // 'swaggerUi',
            // 'ngCookies',
            // 'ui.bootstrap',
            // 'rzModule',
            // 'chart.js',
            // 'pascalprecht.translate',
            // 'angular.filter',
            // 'ngMap',
            // 'ngAnimate',
            // 'mwl.calendar',
            // 'ngFileUpload',
            // 'ngSanitize'
            //'ui.grid',
        ]);

    // ui.router = https://github.com/angular-ui/ui-router
    // ngCookies = Angular package
    // ui.bootstrap = https://angular-ui.github.io/bootstrap/
    // rzModule = https://github.com/angular-slider/angularjs-slider
    // chart.js = https://jtblin.github.io/angular-chart.js/
    // pascalprecht.translate = https://github.com/angular-translate/angular-translate
    // angular.filter = https://github.com/a8m/angular-filter
    // ngMap = http://ngmap.github.io
    // mwl.calendar = https://github.com/mattlewis92/angular-bootstrap-calendar
    // ui.grid = https://github.com/angular-ui/ui-grid


    // ngFileUpload = https://github.com/danialfarid/ng-file-upload  REMOVED FOR NOW

})();



//HEAD 
(function(app) {
try { app = angular.module("app"); }
catch(err) { app = angular.module("app", []); }
app.run(["$templateCache", function($templateCache) {
"use strict";

$templateCache.put("app/draft/draft.view.html","<div class=\"row mt-3\">\n" +
    "    <div class=\"col\">\n" +
    "        <div ui-grid=\"vm.skater_data_grid\"\n" +
    "            class=\"grid\"\n" +
    "            ui-grid-selection></div>\n" +
    "    </div>\n" +
    "</div>\n" +
    "\n" +
    "    <div class=\"row mt-3\">\n" +
    "        <div class=\"col-6\">\n" +
    "            <div ui-grid=\"vm.goalie_data_grid\"\n" +
    "                class=\"goalie_grid\"\n" +
    "                ui-grid-selection></div>\n" +
    "\n" +
    "        </div>\n" +
    "        <div class=\"col-6\">\n" +
    "\n" +
    "            <div class=\"row\">\n" +
    "                <div class=\"col-7\">\n" +
    "                    <h3>{{ vm.selected_player.name }}</h3>\n" +
    "                </div>\n" +
    "                <div class=\"col\">\n" +
    "                    <h5>Position: {{ vm.selected_player.pos }} Team: {{ vm.selected_player.team }} </h5>\n" +
    "                </div>\n" +
    "            </div>\n" +
    "            <div ui-grid=\"vm.player_data_grid\"\n" +
    "                class=\"player_grid\"></div>\n" +
    "\n" +
    "            <div class=\"row mt-2\">\n" +
    "                <div class=\"col\">\n" +
    "                    <button class=\"btn btn-outline-primary btn-sm\"\n" +
    "                        ng-click=\"vm.remove_player(vm.selected_player)\">Remove Player</button>\n" +
    "\n" +
    "                    <button class=\"btn btn-outline-primary btn-sm\"\n" +
    "                        ng-click=\"vm.refresh_draft_results()\">Refresh Draft Results</button>\n" +
    "\n" +
    "                    <button class=\"btn btn-outline-primary btn-sm float-right\"\n" +
    "                        ng-click=\"vm.reset_players()\">Reset All</button>\n" +
    "\n" +
    "                </div>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"row mt-3\">\n" +
    "        <div class=\"col\">\n" +
    "            <div ui-grid=\"vm.target_data_grid\"\n" +
    "                class=\"target_grid\"></div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "    <div class=\"row mt-3 mb-5\">\n" +
    "        <div class=\"col\">\n" +
    "            <div ui-grid=\"vm.team_data_grid\"\n" +
    "                class=\"team_grid\"></div>\n" +
    "        </div>\n" +
    "    </div>")

$templateCache.put("app/home/home.view.html","<div class=\"row mt-3\">\n" +
    "    <div class=\"col-2\">\n" +
    "        <a class=\"btn btn-outline-primary\"\n" +
    "           ui-sref=\"draft\">Go To Draft</a>\n" +
    "    </div>\n" +
    "    <div class=\"col\">\n" +
    "        <a class=\"btn btn-outline-primary\"\n" +
    "           ui-sref=\"hot_players\">Find Hot Players</a>\n" +
    "    </div>\n" +
    "</div>")

$templateCache.put("app/hot_players/hot_players.view.html","<div class=\"row mt-3\">\n" +
    "    <div class=\"col-2\">\n" +
    "        <button class=\"btn btn-sm btn-outline-primary\"\n" +
    "                ng-click=\"vm.refresh_game_stats()\">Refresh Game Stats\n" +
    "            <div status=vm.show.refresh_game_stats></div>\n" +
    "        </button>\n" +
    "    </div>\n" +
    "    <div class=\"col-3\">\n" +
    "        <div class=\"input-group input-group-sm mb-3\">\n" +
    "            <div class=\"input-group-prepend\">\n" +
    "                <span class=\"input-group-text\">GP</span>\n" +
    "            </div>\n" +
    "            <input type=\"text\"\n" +
    "                   ng-model=\"vm.hot_streak_gp\"\n" +
    "                   class=\"form-control\"\n" +
    "                   size=\"2\"\n" +
    "                   placeholder=\"\"\n" +
    "                   aria-label=\"\"\n" +
    "                   aria-describedby=\"basic-addon1\">\n" +
    "            <div class=\"input-group-append\">\n" +
    "                <button class=\"btn btn-outline-primary\"\n" +
    "                        type=\"button\"\n" +
    "                        ng-click=\"vm.get_hot_streaks()\">Refresh Hot Streaks\n" +
    "                    <div status=vm.show.refresh_hot_streak></div>\n" +
    "                </button>\n" +
    "            </div>\n" +
    "        </div>\n" +
    "    </div>\n" +
    "\n" +
    "</div>\n" +
    "\n" +
    "<div class=\"row mt-3\">\n" +
    "    <div class=\"col\">\n" +
    "        <div ui-grid=\"vm.hot_streak_grid\"\n" +
    "             class=\"grid\"\n" +
    "             ui-grid-selection></div>\n" +
    "    </div>\n" +
    "</div>")

$templateCache.put("app/directives/status/status.part.html","<span>\n" +
    "    <i ng-show=\"status==1 || status==0\"\n" +
    "       ng-class=\"{'invisible': status==0}\"\n" +
    "       class=\"fas fa-spinner fa-pulse\"></i>\n" +
    "    <i ng-show=\"status==2\"\n" +
    "       class=\"far fa-check-circle\"></i>\n" +
    "    <i ng-show=\"status==3\"\n" +
    "       class=\"far fa-times-circle\"></i>\n" +
    "</span>")
}]);
})();
(function () {
    'use strict';

    angular
        .module('app')
        .config(rootRouter);

    rootRouter.$inject = ['$stateProvider', '$urlRouterProvider'];
    function rootRouter($stateProvider, $urlRouterProvider) {

        $urlRouterProvider.otherwise('/home');
        $urlRouterProvider.when('', '/home');

        $stateProvider

            .state('home', {
                url: '/home',
                templateUrl: STATIC_ROOT + 'home/home.view.html',
                controller: 'HomeController',
                controllerAs: 'vm',
                // resolve: {
                // Get Token
                //                     Token: ['AuthService', function(AuthService) {
                // //                        AuthService.ValidateToken();
                //                         AuthService.SetTokenRefreshTimer();
                //                         return AuthService.RefreshToken();
                //                     }],
                //                     // Get user information
                //                     User: ['Token', 'UserService',function(Token, UserService) {
                //                         return UserService.GetMyInformation();
                //                     }],
                //                     // Set User Settings
                //                     Settings: ['User', 'UserService', function(User, UserService) {
                //                         return UserService.SetDefaultSettings();
                //                     }]
                // }
            })

            .state('draft', {
                url: '/draft',
                // parent: 'customer_list',
                templateUrl: STATIC_ROOT + 'draft/draft.view.html',
                controller: 'DraftController',
                controllerAs: 'vm',
            })

            .state('hot_players', {
                url: '/hot_players',
                // parent: 'nav',
                templateUrl: STATIC_ROOT + 'hot_players/hot_players.view.html',
                controller: 'HotPlayersController',
                controllerAs: 'vm'
            })

        ;

    }
})();

(function () {
    'use strict';

    angular
        .module('app')
        .controller('DraftController', DraftController);

    DraftController.$inject = ['DataService', 'uiGridConstants'];
    function DraftController(DataService, uiGridConstants) {
        var vm = this;
        if (DEBUG) {
            console.log(vm);
        }

        vm.manual_team_id = DataService.manual_team_id;

        vm.set_visibility = DataService.set_visibility;
        vm.reset_visibility = DataService.reset_visibility;

        vm.team = DataService.team_data;
        vm.data = DataService.player_data;
        vm.draft_resutls = DataService.draft_resutls;
        vm.selected_player = {};

        vm.skater_data_grid = {
            data: DataService.skater_data,
            enableFiltering: true,
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            enableColumnMenus: false,
            multiSelect: false,
            modifierKeysToMultiSelect: false,
            noUnselect: true,
            enableHorizontalScrollbar: 0,
            useExternalSorting: false,
            onRegisterApi: function (gridApi) {
                gridApi.selection.on.rowSelectionChanged(null, on_skater_change);
                vm.grid_api_skater = gridApi;
            },

            columnDefs: [
                { name: 'Name', field: 'name', width: 150 },
                { name: 'Pos', field: 'pos', headerCellClass: 'header_text' },
                { name: 'Team', field: 'team', headerCellClass: 'header_text' },

                { name: 'GP', displayName: 'GP', field: 'gp', type: 'number', cellTemplate: field_template('gp'), enableFiltering: true, headerCellClass: 'bg-gp header_text', sortDirectionCycle: [uiGridConstants.DESC], filters: [{ condition: uiGridConstants.filter.GREATER_THAN, placeholder: 'gt' }] },
                { name: 'G', displayName: 'G', field: 'g', type: 'number', cellTemplate: stat_template('g'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'A', displayName: 'A', field: 'a', type: 'number', cellTemplate: stat_template('a'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: '+/-', displayName: '+/-', field: 'plus_minus', type: 'number', cellTemplate: stat_template('plus_minus'), enableFiltering: false, headerCellClass: 'bg-plus_minus header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PIM', displayName: 'PIM', field: 'pim', type: 'number', cellTemplate: stat_template('pim'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PPP', displayName: 'PPP', field: 'ppp', type: 'number', cellTemplate: stat_template('ppp'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'SHP', displayName: 'SHP', field: 'shp', type: 'number', cellTemplate: stat_template('shp'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GWG', displayName: 'GWG', field: 'gwg', type: 'number', cellTemplate: stat_template('gwg'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'HIT', displayName: 'HIT', field: 'hit', type: 'number', cellTemplate: stat_template('hit'), enableFiltering: false, headerCellClass: 'bg-hit header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'BLK', displayName: 'BLK', field: 'blk', type: 'number', cellTemplate: stat_template('blk'), enableFiltering: false, headerCellClass: 'bg-blk header_text', sortDirectionCycle: [uiGridConstants.DESC] },

                { name: 'TS', displayName: 'TS', field: 'total', type: 'number', cellTemplate: field_template('total'), enableFiltering: false, headerCellClass: 'header_text', defaultSort: { direction: uiGridConstants.DESC, priority: 0 }, sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'OS', displayName: 'OS', field: 'ofence', type: 'number', cellTemplate: field_template('ofence'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'DS', displayName: 'DS', field: 'defence', type: 'number', cellTemplate: field_template('defence'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PS', displayName: 'PS', field: 'points', type: 'number', cellTemplate: field_template('points'), enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
            ]
        };

        vm.goalie_data_grid = {
            data: DataService.goalie_data,
            enableFiltering: true,
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            enableColumnMenus: false,
            multiSelect: false,
            modifierKeysToMultiSelect: false,
            noUnselect: true,
            enableHorizontalScrollbar: 0,
            onRegisterApi: function (gridApi) {
                gridApi.selection.on.rowSelectionChanged(null, on_goalie_change);
                vm.grid_api_goalie = gridApi;
            },
            columnDefs: [

                { name: 'Name', field: 'name', width: 150 },
                { name: 'Pos', field: 'pos', width: 40, enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'Team', field: 'team', enableFiltering: false, headerCellClass: 'header_text' },

                { name: 'GP', displayName: 'GP', field: 'gp', type: 'number', width: 35, enableFiltering: false, cellTemplate: field_template('gp'), headerCellClass: 'bg-gp header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GS', displayName: 'GS', field: 'gs', type: 'number', width: 40, enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'W', displayName: 'W', field: 'w', type: 'number', width: 40, enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GAA', displayName: 'GAA', field: 'gaa', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.ASC] },
                { name: 'SV%', displayName: 'SV%', field: 'sv_p', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'SHO', displayName: 'SHO', field: 'sho', type: 'number', width: 45, enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'W%', displayName: 'W%', field: 'w_p', type: 'number', width: 60, cellTemplate: field_template('w_p'), enableFiltering: false, headerCellClass: 'header_text', defaultSort: { direction: uiGridConstants.DESC } },
            ]
        };

        vm.player_data_grid = {
            data: [],
            enableFiltering: false,
            enableSorting: false,
            enableColumnMenus: false,
            enableHorizontalScrollbar: 0,
            enableVerticalScrollbar: 0,
            onRegisterApi: function (gridApi) {
                vm.grid_api = gridApi;
            },
            columnDefs: []
        };

        vm.columns = {
            skater: [
                { name: 'Year', field: 'year', cellClass: 'header_text', width: 45, sort: { direction: uiGridConstants.DESC, priority: 0, }, headerCellClass: 'bg-gp header_text' },
                { name: 'GP', displayName: 'GP', field: 'gp', width: 35, cellTemplate: field_template('gp'), enableFiltering: false, headerCellClass: 'bg-gp header_text' },
                { name: 'G', displayName: 'G', field: 'g', cellTemplate: stat_template('g'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'A', displayName: 'A', field: 'a', cellTemplate: stat_template('a'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: '+/-', displayName: '+/-', field: 'plus_minus', cellTemplate: stat_template('plus_minus'), enableFiltering: false, headerCellClass: 'bg-plus_minus header_text' },
                { name: 'PIM', displayName: 'PIM', field: 'pim', cellTemplate: stat_template('pim'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'PPP', displayName: 'PPP', field: 'ppp', cellTemplate: stat_template('ppp'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'SHP', displayName: 'SHP', field: 'shp', cellTemplate: stat_template('shp'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'GWG', displayName: 'GWG', field: 'gwg', cellTemplate: stat_template('gwg'), enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'HIT', displayName: 'HIT', field: 'hit', cellTemplate: stat_template('hit'), enableFiltering: false, headerCellClass: 'bg-hit header_text' },
                { name: 'BLK', displayName: 'BLK', field: 'blk', cellTemplate: stat_template('blk'), enableFiltering: false, headerCellClass: 'bg-blk header_text' },

            ],
            goalie: [
                { name: 'Year', field: 'year', cellClass: 'header_text', width: 45, sort: { direction: uiGridConstants.DESC, priority: 0, }, headerCellClass: 'bg-gp header_text' },
                { name: 'GP', displayName: 'GP', field: 'gp', width: 35, cellTemplate: field_template('gp'), enableFiltering: false, headerCellClass: 'bg-gp header_text' },
                { name: 'GS', displayName: 'GS', field: 'gs', enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'W', displayName: 'W', field: 'w', enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'GAA', displayName: 'GAA', field: 'gaa', enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'SV%', displayName: 'SV%', field: 'sv_p', enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'SHO', displayName: 'SHO', field: 'sho', enableFiltering: false, headerCellClass: 'header_text' },
                { name: 'W%', displayName: 'W%', field: 'w_avg', cellTemplate: field_template('w_avg'), enableFiltering: false, headerCellClass: 'header_text' },
            ]
        };

        vm.team_data_grid = {
            data: DataService.team_data,
            enableFiltering: false,
            enableSorting: true,
            enableColumnMenus: false,
            enableHorizontalScrollbar: 0,
            enableVerticalScrollbar: 0,
            onRegisterApi: function (gridApi) {
                vm.grid_api_team = gridApi;
            },
            columnDefs: [
                { name: 'Name', field: 'nick_name', width: 180 },

                { name: 'GP', displayName: 'GP', field: 'gp_skater', type: 'number', width: 50, enableFiltering: false, cellTemplate: field_template('gp_skater'), headerCellClass: 'bg-gp header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'G', displayName: 'G', field: 'g', type: 'number', enableFiltering: false, cellTemplate: rank_template('g'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'A', displayName: 'A', field: 'a', type: 'number', enableFiltering: false, cellTemplate: rank_template('a'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: '+/-', displayName: '+/-', field: 'plus_minus', type: 'number', enableFiltering: false, cellTemplate: rank_template('plus_minus'), headerCellClass: 'bg-plus_minus header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PIM', displayName: 'PIM', field: 'pim', type: 'number', enableFiltering: false, cellTemplate: rank_template('pim'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PPP', displayName: 'PPP', field: 'ppp', type: 'number', enableFiltering: false, cellTemplate: rank_template('ppp'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'SHP', displayName: 'SHP', field: 'shp', type: 'number', enableFiltering: false, cellTemplate: rank_template('shp'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GWG', displayName: 'GWG', field: 'gwg', type: 'number', enableFiltering: false, cellTemplate: rank_template('gwg'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'HIT', displayName: 'HIT', field: 'hit', type: 'number', enableFiltering: false, cellTemplate: rank_template('hit'), headerCellClass: 'bg-hit header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'BLK', displayName: 'BLK', field: 'blk', type: 'number', enableFiltering: false, cellTemplate: rank_template('blk'), headerCellClass: 'bg-blk header_text', sortDirectionCycle: [uiGridConstants.DESC] },

                { name: 'GP ', displayName: 'GP', field: 'gp_goalie', type: 'number', width: 50, enableFiltering: false, cellTemplate: field_template('gp_goalie'), headerCellClass: 'bg-gp header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GS', displayName: 'GS', field: 'gs', type: 'number', enableFiltering: false, cellTemplate: rank_template('gs'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'W', displayName: 'W', field: 'w', type: 'number', enableFiltering: false, cellTemplate: rank_template('w'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GAA', displayName: 'GAA', field: 'gaa', type: 'number', enableFiltering: false, cellTemplate: rank_template('gaa'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.ASC] },
                { name: 'SV%', displayName: 'SV%', field: 'sv_p', type: 'number', enableFiltering: false, cellTemplate: rank_template('sv_p'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'SHO', displayName: 'SHO', field: 'sho', type: 'number', enableFiltering: false, cellTemplate: rank_template('sho'), headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
            ]
        };

        vm.target_data_grid = {
            data: [DataService.targets],
            enableFiltering: false,
            enableSorting: true,
            enableColumnMenus: false,
            enableHorizontalScrollbar: 0,
            enableVerticalScrollbar: 0,
            showHeader: false,
            onRegisterApi: function (gridApi) {
                vm.grid_api_target = gridApi;
            },
            columnDefs: [
                { name: 'Targets', field: 'name', width: 180 },

                { name: 'GP', field: 'gp_skater', type: 'number', width: 50, enableFiltering: false, cellTemplate: field_template('gp_skater'), },
                { name: 'G', field: 'g', type: 'number', enableFiltering: false, },
                { name: 'A', field: 'a', type: 'number', enableFiltering: false, },
                { name: '+/-', field: 'plus_minus', type: 'number', enableFiltering: false, cellTemplate: field_template('plus_minus'), sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PIM', field: 'pim', type: 'number', enableFiltering: false, },
                { name: 'PPP', field: 'ppp', type: 'number', enableFiltering: false, },
                { name: 'SHP', field: 'shp', type: 'number', enableFiltering: false, },
                { name: 'GWG', field: 'gwg', type: 'number', enableFiltering: false, },
                { name: 'HIT', field: 'hit', type: 'number', enableFiltering: false, cellTemplate: field_template('hit'), },
                { name: 'BLK', field: 'blk', type: 'number', enableFiltering: false, cellTemplate: field_template('blk'), },

                { name: 'GP ', field: 'gp_goalie', type: 'number', width: 50, enableFiltering: false, cellTemplate: field_template('gp_goalie'), },
                { name: 'GS', field: 'gs', type: 'number', enableFiltering: false, },
                { name: 'W', field: 'w', type: 'number', enableFiltering: false, },
                { name: 'GAA', field: 'gaa', type: 'number', enableFiltering: false, },
                { name: 'SV%', field: 'sv_p', type: 'number', enableFiltering: false, },
                { name: 'SHO', field: 'sho', type: 'number', enableFiltering: false, },
            ]
        };

        function field_template(field) {
            return '<div class="ui-grid-cell-contents bg-' + field + '" ng-class="{\'bg-good\':row.entity.' + field + '_trend==1, \'bg-bad\':row.entity.' + field + '_trend==3}" >{{ row.entity.' + field + ' }}</div>';
        }

        function stat_template(field) {
            return '<p class="ui-grid-cell-contents ui-grid-cell-contents-compact bg-' + field + '" ng-class="{\'bg-good\':row.entity.' + field + '_trend==1, \'bg-bad\':row.entity.' + field + '_trend==3}" >' +
                '       <span style="font-size: 90%;">{{ row.entity.' + field + ' }}</span><br>' +
                '       <span style="font-size: 60%;">{{ row.entity.' + field + '_weight | number:4 }}</span>' +
                '   </p>';
        }

        function rank_template(field) {
            return '<p class="ui-grid-cell-contents ui-grid-cell-contents-compact bg-' + field + '" >' +
                '       <span style="font-size: 90%;">{{ row.entity.' + field + ' }}</span><br>' +
                '       <span style="font-size: 60%;">rnk:{{ row.entity.' + field + '_rank }} avg:<span class="text-danger">{{ row.entity.' + field + '_avg_rank }}</span></span>' +
                '   </p>';
        }

        function on_skater_change(row) {
            on_row_selection_change(row, 'skater');
        }
        function on_goalie_change(row) {
            on_row_selection_change(row, 'goalie');
        }

        function on_row_selection_change(row, type) {
            var id = row.entity.id;

            angular.forEach(vm.data, function (item) {
                if (item.id == id) {
                    var stats = angular.copy(item.season_stats);
                    stats.pop();// remove trends entry

                    vm.player_data_grid.data = stats;
                    vm.player_data_grid.columnDefs = vm.columns[type];
                    vm.grid_api.core.notifyDataChange(uiGridConstants.dataChange.ALL);

                    vm.selected_player = angular.copy(item);// binding must be broken
                }
            });
        }

        function remove_player(data) {
            if ('id' in data) {
                DataService.set_visibility(data.id, false).then(
                    function (response) {
                        var pos = 'skater';
                        var player = vm.data[data.id];

                        player.visible = false;
                        if (player.pos == 'G') { pos = 'goalie'; }

                        angular.forEach(DataService[pos + '_data'], function (item, index) {
                            if (item.id == response.data.player_id) {
                                DataService[pos + '_data'].splice(index, 1);
                                vm['grid_api_' + pos].core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                            }
                        });

                        enpty_dict(vm.selected_player);
                        enpty_dict(vm.player_data_grid.data);
                        vm.grid_api.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                    }
                );
            }
        }
        vm.remove_player = remove_player;

        function reset_players() {
            DataService.reset_visibility().then(
                function () {
                    angular.forEach(vm.data, function (item) {
                        item.status.visible = true;
                        item.status.taken = false;
                    });
                    DataService.populate_players_for_tables();
                    vm.grid_api_skater.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                    vm.grid_api_goalie.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                }
            );
        }
        vm.reset_players = reset_players;

        function refresh_draft_results() {

            if (vm.bypass) {
                DataService.get_draft_results().then(
                    function () {
                        vm.grid_api_skater.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                        vm.grid_api_goalie.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                        vm.grid_api_team.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                    }
                );
            } else {
                DataService.refresh_and_get_draft_results().then(
                    function () {
                        vm.grid_api_skater.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                        vm.grid_api_goalie.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                        vm.grid_api_team.core.notifyDataChange(uiGridConstants.dataChange.OPTIONS);
                    }
                );
            }




        }
        vm.refresh_draft_results = refresh_draft_results;

        function enpty_dict(dict) {
            for (var prop in dict) {
                if (dict.hasOwnProperty(prop)) {
                    delete dict[prop];
                }
            }
        }

        init();

        ////////////////

        function init() { }
    }
})();
(function () {
    'use strict';

    angular
        .module('app')
        .controller('RootController', RootController);

    RootController.$inject = ['DataService'];
    function RootController(DataService) {
        var vm = this;
        if (DEBUG) {
            console.log(vm);
        }

        vm.loading = DataService.loading;


        init();

        ////////////////

        function init() { }
    }
})();


(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    // HomeController.$inject = ['DataService'];
    function HomeController() {
        var vm = this;
        if (DEBUG) {
            console.log(vm);
        }

        init();

        ////////////////

        function init() { }
    }
})();
(function () {
    'use strict';

    angular
        .module('app')
        .factory('DataService', DataService);

    DataService.$inject = ['$http', '$q'];
    function DataService($http, $q) {
        var service = {
            loading: { loading: true },

            reset_visibility: reset_visibility,
            set_visibility: set_visibility,
            populate_players_for_tables: populate_players_for_tables,

            get_draft_results: get_draft_results,
            refresh_and_get_draft_results: refresh_and_get_draft_results,

            player_data: {},
            team_player_data: {},
            skater_data: [],
            goalie_data: [],
            draft_resutls: [],
            teams: {},
            team_data: [],
            targets: { // backup targets
                g: 315,
                a: 501,
                plus_minus: 90,
                pim: 660,
                ppp: 273,
                shp: 17,
                gwg: 57,
                hit: 1600,
                blk: 980,
                gs: 131,
                w: 84,
                gaa: 2.11,
                sv_p: 0.925,
                sho: 14,
            },

            manual_team_id: '386.l.2131.t.3',

            // game stats section
            refresh_game_stats: refresh_game_stats,
            get_hot_streaks: get_hot_streaks,
        };

        init();
        return service;

        function init() {
            // console.log(service);            
            get_team_data();
            get_team_players().then(
                function () {
                    get_targets().then(
                        function () {
                            get_player_data().then(// requires target & team_players
                                function () {
                                    service.loading.loading = false;
                                }
                            );
                        }
                    );
                }
            );

        }

        ////////////////
        function get_player_data() {
            var url = API_ROOT + 'player/';
            return http('GET', url).then(
                function (response) {
                    angular.forEach(response.data, function (item) {
                        this[item.id] = item;
                    }, service.player_data);
                    process_data(service.player_data);
                    return response;
                }
            );
        }

        function get_team_players() {
            var url = API_ROOT + 'team_players/';
            return http('GET', url).then(
                function (response) {
                    angular.forEach(response.data, function (item) {
                        this[item.player_id] = item.team_id;
                    }, service.team_player_data);
                    return response;
                }
            );
        }

        function get_team_data() {
            var url = API_ROOT + 'team/';
            http('GET', url).then(
                function (response) {
                    angular.forEach(response.data, function (item) {
                        this[item.id] = item;
                    }, service.teams);
                }
            );

        }

        function get_draft_results() {
            var results = $q.defer();

            var request = function () {
                var url = API_ROOT + 'draft_results/';
                http('GET', url).then(
                    function (response) {
                        service.draft_resutls.splice(0, service.draft_resutls.length);
                        angular.forEach(response.data, function (item) {
                            this.push(item);
                        }, service.draft_resutls);
                        players_update_taken();
                        results.resolve(true);
                    }
                );

            };
            request();
            return results.promise;
        }

        function refresh_and_get_draft_results() {
            var results = $q.defer();

            var request = function () {
                var url = API_ROOT + 'draft_results/';
                http('PATCH', url).then(
                    function (response) {
                        if (response.status == 200) {
                            get_draft_results().then(
                                function () {
                                    results.resolve(true);
                                }
                            );
                        }
                    }
                );

            };
            request();
            return results.promise;
        }

        function get_targets() {
            var url = API_ROOT + 'target/';
            return http('GET', url).then(
                function (response) {
                    var current_year = (new Date()).getFullYear();

                    angular.forEach(response.data, function (target) {
                        if (target.league.year == current_year) {
                            angular.forEach(target, function (val, key) {

                                this[key] = val;
                            }, service.targets);
                        }
                    });
                    service.targets.name = 'Targets';

                    return response;
                }
            );

        }

        function players_update_taken() {
            clear_mine();
            if (Object.keys(service.player_data).length > 0) {
                angular.forEach(service.draft_resutls, function (item) {
                    if (item.player_id && item.player_id in service.player_data) {
                        service.player_data[item.player_id].status.taken = true;
                        if (item.team_id == service.manual_team_id) {
                            service.player_data[item.player_id].mine = true;
                        }
                    }
                });
                // compiling values for table
                populate_players_for_tables();
                compile_teams_results();
            }
        }

        function clear_mine() {
            angular.forEach(service.player_data, function (item) {
                item.mine = false;
            });

        }

        function process_data(player_data) {
            //////////////////////////////////////////////
            // adding a bunch of calculated fields to player season stats
            //////////////////////////////////////////////
            var year = (new Date()).getFullYear() - 1;
            var stat_cols = ['g', 'a', 'plus_minus', 'pim', 'ppp', 'shp', 'gwg', 'hit', 'blk', 'w'];
            var trend_cols = stat_cols.concat(['total', 'ofence', 'defence', 'points']);
            var weight_group = {
                'total': ['g', 'a', 'plus_minus', 'pim', 'ppp', 'shp', 'gwg', 'hit', 'blk'],
                'ofence': ['g', 'a', 'plus_minus', 'ppp', 'gwg'],
                'defence': ['plus_minus', 'pim', 'shp', 'hit', 'blk'],
                'points': ['g', 'a', 'ppp'],
            };
            angular.forEach(player_data, function (row) {
                // adding values to row
                row.owner_id = service.team_player_data[row.id];// not used for draft but for tracking
                row.owner_name = row.owner_id ? service.teams[row.owner_id].nick_name : '';

                row.status.taken = false;// prep for draft_results
                row.pos = make_pos(row);
                angular.forEach(row.season_stats, function (stat) {
                    stat.toi = adjust_toi(stat.toi);// toi str to number
                    // set avg and weight
                    angular.forEach(stat_cols, function (item) {
                        if (stat[item] == null) { stat[item] = 0; }
                        stat[item + '_avg'] = stat.gp == 0 ? 0 : stat[item] / stat.gp;
                        stat[item + '_weight'] = stat[item + '_avg'] / (service.targets[item] / 1000);
                        stat[item + '_avg'] = Math.round(stat[item + '_avg'] * 1000) / 1000;
                    });

                    // set weight groups
                    angular.forEach(weight_group, function (val, key) {
                        stat[key] = 0;
                        angular.forEach(val, function (item) {
                            stat[key] += stat[item] == 0 ? 0 : stat[item + '_weight'];
                        });
                        stat[key] = Math.round(stat[key] * 1000) / 1000;
                        stat[key + '_weight'] = stat[key];
                    });

                });

                // add trend values based on last 3 years
                var trends = { year: 'trend' };
                angular.forEach(trend_cols, function (item) {
                    var y1 = get_stat(row, year, item + '_weight');
                    var y2 = get_stat(row, year - 1, item + '_weight');
                    var y3 = get_stat(row, year - 2, item + '_weight');
                    if (y1 > y2 && y2 > y3) {
                        this[item] = 1;//good
                    }
                    else if (y1 > y2 && y2 < y3) {
                        this[item] = 2;//medium
                    }
                    else if (y1 < y2 && y2 < y3) {
                        this[item] = 3;//bad
                    }
                    else {
                        this[item] = 0;//not worth mentioning
                    }
                }, trends);
                row.season_stats.push(trends);

            });
            // compiling values for table
            populate_players_for_tables();
        }

        function populate_players_for_tables() {
            var year = (new Date()).getFullYear() - 1;
            var stat_cols = ['g', 'a', 'plus_minus', 'pim', 'ppp', 'shp', 'gwg', 'hit', 'blk', 'w'];
            var trend_cols = stat_cols.concat(['total', 'ofence', 'defence', 'points']);

            service.skater_data.splice(0, service.skater_data.length);
            service.goalie_data.splice(0, service.goalie_data.length);

            angular.forEach(service.player_data, function (row, key) {
                if (row.status.visible && !row.status.taken) {
                    var d = {
                        id: key,
                        name: row.name,
                        pos: row.pos,
                        team: row.team,
                        mine: row.mine,
                        gp: get_stat(row, year, 'gp'),
                    };

                    if (row.pos_g) {// if goalie
                        d.gs = get_stat(row, year, 'gs');
                        d.w = get_stat(row, year, 'w');
                        d.gaa = get_stat(row, year, 'gaa');
                        d.sv_p = get_stat(row, year, 'sv_p');
                        d.sho = get_stat(row, year, 'sho');
                        d.w_p = get_stat(row, year, 'w_avg');
                        d.w_p = Math.round(d.w_p * 1000) / 1000;

                        d.w_p_trend = get_stat(row, 'trend', 'w');

                        service.goalie_data.push(d);

                    } else {// if skater
                        angular.forEach(trend_cols, function (item) {
                            this[item] = get_stat(row, year, item);
                            this[item + '_weight'] = get_stat(row, year, item + '_weight');
                            this[item + '_trend'] = get_stat(row, 'trend', item);
                        }, d);

                        service.skater_data.push(d);
                    }
                }
            });
        }

        function compile_teams_results() {
            var year = (new Date()).getFullYear() - 1;
            var sum_cols = ['g', 'a', 'plus_minus', 'pim', 'ppp', 'shp', 'gwg', 'hit', 'blk', 'gs', 'w', 'toi', 'ga', 'sa', 'sv', 'sho'];
            var rank_cols = sum_cols.concat(['gaa', 'sv_p']);

            // set init values
            angular.forEach(service.teams, function (team) {
                angular.forEach(sum_cols, function (item) {
                    this[item] = 0;
                }, team);
                team.gp_skater = 0;
                team.gp_goalie = 0;
            });

            // compile stats
            angular.forEach(service.draft_resutls, function (item) {
                if (item.player_id in service.player_data) {
                    var team = service.teams[item.team_id];// for testing add .replace('old_id', 'new_id')
                    var player = service.player_data[item.player_id];
                    var player_stat;
                    angular.forEach(player.season_stats, function (stat) {
                        if (stat.year == year) { player_stat = stat; }
                    });

                    if (player.pos == 'G') {
                        team.gp_goalie += player_stat.gp;
                    } else {
                        team.gp_skater += player_stat.gp;
                    }
                    angular.forEach(sum_cols, function (col) {
                        team[col] += player_stat[col];
                        team[col + '_avg'] = team[col] / (player.pos == 'G' ? team.gp_goalie : team.gp_skater);// set average for ranking
                    });

                }
            });

            // add gaa and sv_p
            angular.forEach(service.teams, function (team) {
                team.gaa = Math.round((team.ga / (team.toi / 60)) * 100) / 100;
                team.sv_p = Math.round((team.sv / team.sa) * 1000) / 1000;
                team.gaa_avg = team.gaa;
                team.sv_p_avg = team.sv_p;
            });

            // add ranking
            var ranks = {};
            angular.forEach(rank_cols, function (field) {
                ranks[field] = [];
                ranks[field + '_avg'] = [];

                angular.forEach(service.teams, function (team) {
                    this[field].push(team[field]);
                    this[field + '_avg'].push(team[field + '_avg']);
                }, ranks);
            });

            var sorted_ranks = {};
            angular.forEach(rank_cols, function (field) {
                if (field == 'gaa') {
                    this[field] = ranks[field].slice().sort(function (a, b) { return a - b; });
                    this[field + '_avg'] = ranks[field + '_avg'].slice().sort(function (a, b) { return a - b; });
                } else {
                    this[field] = ranks[field].slice().sort(function (a, b) { return b - a; });
                    this[field + '_avg'] = ranks[field + '_avg'].slice().sort(function (a, b) { return b - a; });
                }
            }, sorted_ranks);

            angular.forEach(rank_cols, function (field) {
                angular.forEach(service.teams, function (team) {
                    team[field + '_rank'] = sorted_ranks[field].indexOf(team[field]) + 1;
                    team[field + '_avg_rank'] = sorted_ranks[field + '_avg'].indexOf(team[field + '_avg']) + 1;
                });
            });

            //set team_data
            service.team_data.splice(0, service.team_data.length);
            angular.forEach(service.teams, function (team) {
                this.push(team);
            }, service.team_data);
        }

        function make_pos(row) {
            var str = '';
            if (row.pos_c) { str += ',C'; }
            if (row.pos_w) { str += ',W'; }
            if (row.pos_d) { str += ',D'; }
            if (row.pos_g) { str += ',G'; }
            return str.substring(1);
        }

        function adjust_toi(toi) {
            // take toi str, return toi integer
            if (toi) {
                var data = toi.split(':');
                data[0] = parseInt(data[0].replace(',', ''));
                data[1] = parseFloat(data[1] / 60);
                return data[0] + data[1];

            } else {
                return null;
            }
        }

        function get_stat(row, year, stat_name) {
            for (var i in row.season_stats) {
                if (row.season_stats[i].year == year) {
                    return row.season_stats[i][stat_name];
                }
            }
        }

        function set_visibility(player_id, visible) {
            var url = API_ROOT + 'player/' + player_id + '/status/';
            var data = {
                visible: visible
            };

            return http('PUT', url, data);
        }

        function reset_visibility() {
            var url = API_ROOT + 'status_reset/';
            return http('GET', url);
        }

        function refresh_game_stats() {
            var url = API_ROOT + 'game_stats/';
            return http('PATCH', url);
        }

        function get_hot_streaks(game_count) {
            var url = API_ROOT + 'reports/hot_streak/';
            var params = { game_count: game_count };
            return http('POST', url, params);
        }


        function http(method, url, data, params) {
            var results = $q.defer();
            var request = function () {
                $http({
                    method: method,
                    url: url,
                    data: data,
                    params: params,
                }).then(
                    function (response) {
                        results.resolve(response);
                    }, function (error) {
                        console.error(error);
                        results.reject(error);
                    }
                );
            };
            request();
            return results.promise;
        }
    }
})();


(function () {
    'use strict';

    angular
        .module('app')
        .controller('HotPlayersController', HotPlayersController);

    HotPlayersController.$inject = ['DataService', 'uiGridConstants'];
    function HotPlayersController(DataService, uiGridConstants) {
        var vm = this;
        if (DEBUG) {
            console.log(vm);
        }

        vm.hot_streak_gp = 4;

        function refresh_game_stats() {
            show('refresh_game_stats', 1);
            DataService.refresh_game_stats().then(
                function () {
                    show('refresh_game_stats', 2);
                },
                function () {
                    show('refresh_game_stats', 3);
                }
            );
        }
        vm.refresh_game_stats = refresh_game_stats;

        vm.hot_streak_data = [];
        function get_hot_streaks() {
            var game_count = vm.hot_streak_gp;
            show('refresh_hot_streak', 1);

            DataService.get_hot_streaks(game_count).then(
                function (response) {
                    vm.hot_streak_data.splice(0, vm.hot_streak_data.length);
                    // console.log(response);
                    angular.forEach(response.data, function (val, key) {
                        val.name = get_player_info(key, 'name');
                        val.team = get_player_info(key, 'team');
                        val.pos = get_player_info(key, 'pos');
                        val.owner_name = get_player_info(key, 'owner_name');
                        if (val.pos != 'G') {
                            this.push(val);
                        }
                    }, vm.hot_streak_data);
                    show('refresh_hot_streak', 2);
                },
                function () {
                    show('refresh_hot_streak', 3);
                }
            );
        }
        vm.get_hot_streaks = get_hot_streaks;


        function get_player_info(player_id, field) {
            return DataService.player_data[player_id][field];
        }




        /////////////////////// tables
        vm.hot_streak_grid = {
            data: vm.hot_streak_data,
            enableFiltering: true,
            enableRowSelection: true,
            enableRowHeaderSelection: false,
            enableColumnMenus: false,
            multiSelect: false,
            modifierKeysToMultiSelect: false,
            noUnselect: true,
            enableHorizontalScrollbar: 0,
            useExternalSorting: false,
            // onRegisterApi: function (gridApi) {
            //     // gridApi.selection.on.rowSelectionChanged(null, on_skater_change);
            //     // vm.grid_hot_streak = gridApi;
            // },

            columnDefs: [
                index_row(3),
                { name: 'Name', field: 'name', width: 150 },
                { name: 'Pos', field: 'pos', headerCellClass: 'header_text' },
                { name: 'Team', field: 'team', headerCellClass: 'header_text' },
                { name: 'Owner', field: 'owner_name', headerCellClass: 'header_text', width: 150 },

                { name: 'GP', displayName: 'GP', field: 'gp', type: 'number', cellTemplate: stat_template('gp'), enableFiltering: true, headerCellClass: 'bg-gp header_text', sortDirectionCycle: [uiGridConstants.DESC], },
                { name: 'G', displayName: 'G', field: 'g', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'A', displayName: 'A', field: 'a', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: '+/-', displayName: '+/-', field: 'plus_minus', type: 'number', cellTemplate: stat_template('plus_minus'), enableFiltering: false, headerCellClass: 'bg-plus_minus header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PIM', displayName: 'PIM', field: 'pim', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'PPP', displayName: 'PPP', field: 'ppp', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'SHP', displayName: 'SHP', field: 'shp', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'GWG', displayName: 'GWG', field: 'gwg', type: 'number', enableFiltering: false, headerCellClass: 'header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'HIT', displayName: 'HIT', field: 'hit', type: 'number', cellTemplate: stat_template('hit'), enableFiltering: false, headerCellClass: 'bg-hit header_text', sortDirectionCycle: [uiGridConstants.DESC] },
                { name: 'BLK', displayName: 'BLK', field: 'blk', type: 'number', cellTemplate: stat_template('blk'), enableFiltering: false, headerCellClass: 'bg-blk header_text', sortDirectionCycle: [uiGridConstants.DESC] },

            ]
        };



        init();

        ////////////////

        function init() {
            vm.show = {};
        }

        function stat_template(field) {
            return '<p class="ui-grid-cell-contents ui-grid-cell-contents-compact bg-' + field + '" ng-class="{\'bg-good\':row.entity.' + field + '_trend==1, \'bg-bad\':row.entity.' + field + '_trend==3}" >' +
                '       <span style="font-size: 90%;">{{ row.entity.' + field + ' }}</span><br>' +
                '       <span style="font-size: 60%;">{{ row.entity.' + field + '_weight | number:4 }}</span>' +
                '   </p>';
        }

        function index_row(number) {
            return { name: 'index', field: 'index', displayName: '', width: 13 * number, enableFiltering: false, cellTemplate: '<div class="ui-grid-cell-contents">{{grid.renderContainers.body.visibleRowCache.indexOf(row)+1}}</div>' };
        }

        function show(item, val) {
            vm.show[item] = val;
        }





    }
})();

// service.draft_resutls.splice(0, service.draft_resutls.length);
//                         angular.forEach(response.data, function (item) {
//                             this.push(item);
//                         }, service.draft_resutls);
(function () {
    'use strict';

    angular
        .module('app')
        .directive('status', status);

    status.$inject = ['$timeout'];
    function status($timeout) {
        // Usage:
        //
        // Creates:
        //
        var directive = {
            restrict: 'A',
            replace: true,
            scope: {
                status: '=',
            },
            templateUrl: STATIC_ROOT + 'directives/status/status.part.html',
            link: link,
        };
        return directive;

        function link(scope) {
            if (!angular.isDefined(scope.status)) {
                scope.status = 0;
            }

            scope.$watch('status', function (new_val) {
                if (new_val == 2 || new_val == 3) {
                    clear_show();
                }
            });

            function clear_show() {
                $timeout(function () {
                    scope.status = 0;
                }, 1000 * 5);
            }

        }
    }

})();