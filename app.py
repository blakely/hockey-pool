import glob

from apistar import App

from back_end.components import components
from back_end.event_hooks import event_hooks
from back_end.routes import routes
from back_end.settings import FRONTEND_DIR

app = App(routes=routes, components=components, event_hooks=event_hooks, static_dir=FRONTEND_DIR)

extra_files = list(glob.iglob('./front_end/**/*.*', recursive=True))

if __name__ == '__main__':
    app.serve('127.0.0.1', 5000, debug=True, extra_files=extra_files)
