from back_end.lib.db_session import db_session
from back_end.settings import DEBUG
from back_end.yahoo.get_game_schedule import get_all_game_schedule

from back_end.yahoo.get_gm_teams import get_gm_teams
from back_end.yahoo.get_leagues import get_leagues
from back_end.yahoo.get_player_season_stats import get_all_player_season_stats
from back_end.yahoo.get_players import get_all_players
from back_end.yahoo.get_stat_category import get_stat_category, get_tracked_stat_category
from back_end.yahoo.get_targets import get_current_and_recent_targets
from back_end.yahoo.yahoo_session import yahoo_session

ys = yahoo_session()

db_session = db_session()


def init():
    pass
    get_stat_category(ys, db_session)
    get_leagues(ys, db_session)

    get_gm_teams(ys, db_session)
    get_tracked_stat_category(ys, db_session)
    get_current_and_recent_targets(ys, db_session)

    get_all_game_schedule(db_session)

    max_players = None
    if DEBUG:
        max_players = 99
    get_all_players(ys, db_session, max_players=max_players)
    get_all_player_season_stats(ys, db_session)


init()
